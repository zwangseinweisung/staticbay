# qcow2 Disk Image mounten
> 16. Mai 2019 von ragnar

Vor kurzem musste ich mal manue auf ein QCOW2 Disk-Image zugreifen und wusste nicht so recht was ich machen sollte. Dank des Internets wurde ich schnell fündig:



```bash
modprobe nbd max_part=8
qemu-nbd --connect=/dev/nbd0 db1_old.qcow2
# mount first partition
mount /dev/nbd0p1 /mnt/
```

Das Problem war, ich hatte keine Partition namens „nbd0p1“. Aber auch hier half mir das Internet. Ich musste lediglich einen weiteren Befehl einfügen der dem Kernel die vorhanden Partitionen bekannt macht. Dieses kleine kpartx -a war dann enorm wichtig.

Das ganze sieht dann so aus:

```bash
modprobe nbd max_part=8
qemu-nbd --connect=/dev/nbd0 db1_old.qcow2
# mount first partition
kpartx -a
mount /dev/nbd0p1 /mnt/
```

um das ganze dann wieder freizugeben:

```bash
umount  /mnt/
qemu-nbd --disconnect /dev/nbd0
```