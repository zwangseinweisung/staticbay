
# BSOD – Hidden Messages
> 2. Januar 2020 von ragnar

Jeder kennt es, Windows, Linux und Mac sind nicht gegen Abstürze gefeit und hin und wieder stößt man dann auf einen Bildschirm der einen darüber informiert dass das System grade, durch einen kritischen Systemfehler nicht mehr lauffähig ist. So gibt es den „Blue Screen of Death“ unter Windows, den „Sad Mac“ unter dem alten Mac OS, dem „Kernel Panic“ unter Linux oder die TOS-Bomben () auf dem Atari [^1]. Dies hat sich Jamie Zawinski zu nutze gemacht und einen bsod Bildschirmschoner[^2] entwickelt.

Bildschirmschoner? Ja, die Relikte aus den 90ern um zu verhindern das, bei längeren Arbeitspausen, sich der Inhalt in den Monitor einbrennt. Bis auf ein paar Ausnahmen, wie OLED z.B., sind die Dinger aber eigentlich nicht mehr notwendig . Heutzutage dienen die Bildschirmschoner nur noch der Unterhaltung (oder waren z.B. von Anfang an darauf konzipiert wie z.B. Johnny Castaway [^3]).

Einer der Bildschirmschoner ist dem Windows 10 Absturzbildschirm nachempfunden. Normalerweise steht im Windows 10 Absturzbildschirm, außer dem übergroßen Smiley, alle relevanten Daten (die Bug Check Code Reference [^4]), eine Internetadresse und einen QR-Code der auf die selbe Adresse verweist. Die wenigsten werden noch einen weiteren Computer daneben stehen haben und so ist es viel einfacher an die Beschreibung zu kommen, einfach mit dem Handy abknipsen und die URL aufrufen und lesen was dort auf der Webseite steht. Hier hat sich der Autor wohl einen kleinen Spass erlaubt und in den Windows 10 Bildschirm ein paar versteckte Nachrichten eingebaut.

* Anstatt einer aka.ms Adresse steht dort ein Youtube Link drin. Wenn man dem folgt landet man auf einem ziemlich verstörenden Video – youtu.be/-RjmN9RZyr4

* Im QR-Code stehen die wichtigsten Zeilen zu „Never Gonna Give You Up“. Man wird also Rickrolled.

Ob noch weitere Easter Eggs in den Bildschirmschonern stehen weiss ich nicht, ich habe da noch nicht drauf geachtet.

[^1] https://en.wikipedia.org/wiki/Screen_of_death
[^2] https://www.jwz.org/xscreensaver/
[^3] https://en.wikipedia.org/wiki/Johnny_Castaway
[^4] https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/bug-check-code-reference2
