# Office 365 – „gesendete Elemente“ werden nicht da gespeichert wo man sie erwartet
> 15. Mai 2019 von ragnar

Eigentlich sind freigegebene Postfächer in Office 365 bzw. Outlook was feines. Es lässt sich relativ leicht eine Mailadresse anlegen auf die dann verschiedene Benutzer zugreifen können. Das bequeme daran ist das die automatisch in Outlook erscheinen. Leider haben die aber einen kleinen Pferdefuß um den ich mich in diesem Artikel zuwende.

Nehmen wir mal an, wir verwenden Office 365 und benutzen freigegebene Postfächer, dann landen die gesendeten Mails nicht, wie zu erwarten, im Ordner „gesendete Elemente“ des freigegebenen Postfachs sondern in „gesendete Elemente“ des Hauptkontos. Zum Glück hat sich Microsoft schon damit beschäftigt. Daher gibt es dann auch schon eine Lösung:

1. Klicken Sie auf Start und auf Ausführen, geben Sie regedit ein, und klicken Sie auf OK.

2. Klicken Sie auf den folgenden Unterschlüssel in der Registrierung:

3. HKEY_CURRENT_USER\Software\Microsoft\Office\x.0\Outlook\Preferences

4. Hinweis Der Platzhalter x.0 steht für Ihre Version von Office (16.0 = Office 2016, 15.0 = Office 2013, 14.0 = Office 2010).

5. Zeigen Sie im Menü Bearbeiten auf Neu, und klicken Sie anschließend auf DWORD-Wert.

6. Geben Sie DelegateSentItemsStyle ein, und drücken Sie dann die EINGABETASTE.

7. Klicken Sie mit der rechten Maustaste auf DelegateSentItemsStyle, und klicken Sie dann auf Ändern.

8. Geben Sie in das Datenfeld Wert den Wert 1 ein, und klicken Sie dann auf OK.

9. Beenden Sie den Registrierungs-Editor.

Dann noch Outlook neu starten und das wars dann auch schon 🙂

https://learn.microsoft.com/de-DE/exchange/troubleshoot/user-and-shared-mailboxes/sent-mail-is-not-saved