# Ersatz für den Desktoprechner
> 12. Januar 2023 von ragnar

Die Tage hat die kleine Kröte hier meinen Desktoprechner geschrottet. Sie hatte eine Heidenspass daran den Powerbutton immer und immer wieder zu drücken. Eine Zeitlang gings gut bis mir das auf den Keks ging und ih den Taster und die vorderen USB-Anschlüsse abgeklebt habe. Das ging dann schließlich so weit das die Zicke hier anfing die geklebte Abdeckung abzuknibbeln und dann trotzdem wieder auf dem Taster rumdrückte. Long story short: Irgendwas am Rechner hat das ewige ein und ausschalten nicht vertragen und den Dienst quittiert.

Weil ich meinen Desktop nicht nur zum zocken benutze und ich dort wichtige Daten darauf gespeichert habe, musste ich mich also um Ersatz bemühen. Gar nicht so einfach in der aktuelle Zeit. Corona und die damit verbundenen Lieferengpässe auf der einen Seite und eine ziemlich knackige Inflation auf der anderen, schränken die Auswahl sehr ein. Es wird zwar langsam besser mit der Verfügbarkeit aber trotzdem ist noch nicht alles in ausreichender Menge vorhanden.

Meine Hardware kaufe ich egtl. nur bei einem Händler in Düsseldorf (ich will hier keine Namen nennen) daher habe ich dort mit der Suche begonnen und war ersteinmal positiv überrascht. So viel teuerer ist das Zeug gar nicht als sonst, auch waren die meisten Sachen verfügbar sodass ich einige Zeit damit verbracht habe den Onlineshop zu durchsuchen.

Klar war dass ich so viel wie möglich wieder übernehmen möchte um Geld zu sparen. Aber ich wusste nicht was kaputt war. Daher hab ich mich dazu entschlossen zunächst das Mainboard und die CPU zu ersetzen und im falle des Falles noch mehr neue Hardware dazuzukaufen. Da Lüfter umd RAM bereits vorhanden waren, war ich wieder auf AMD ausgerichtet (außerdem haben die das bessere Preis/Leistungsangebot) und weil mein Budget auch nicht das größte war musste ich mich dann auch noch zwangsläufig im unteren Preissegment umsehen.

## Neue Hardware
Aus Spar/- und Verfügbarkeitsgründen habe ich mich dann für folgende Sachen entschieden:

* CPU : AMD Ryzen 5 5600x
* Mainboard: Asus Tuf Gaming X570
* Gehäuse: Be quit! Pure Base 500

Zu den Komponenten im einzelnen

### CPU
Was soll ich schon dazu sagen was andere nicht schon beschrieben haben? Es ist ein 5600X der schon seit gut 4 Jahren (oder so) am Markt ist. Jeder kennt ihn, jeder liebt ihn. Was mich allerdings überrascht hat war das Gewicht. Donnerwetter, der ist echt schwer! Aus Jux hab ich mal einen alten Athlon XP in die andere Hand genommen, ich glaube 4 Athlons wiegen so viel wie ein Ryzen. Ein Vergleich der alten (links) und der neuen (recht) CPU könnt ihr unten in der Tabelle sehen.

||||
|--- |--- |--- |
|AMD Ryzen 5|Familie|AMD Ryzen 5|
|AMD Ryzen 3000|CPU Gruppe|AMD Ryzen 5000|
|3|Generation|4|
|Matisse (Zen 2)|Architektur|Vermeer (Zen 3)|
|6|Kerne|6|
|12|Threads|12|
|Ja|Hyperthreading|Ja|
|3,60 GHz|Taktfrequenz|3,70 GHz|
|4,20 GHz|Turbo Taktfrequenz (1 Kern)|4,60 GHz|
|4,00 GHz|Turbo Taktfrequenz (Alle
Kerne)|4,30 GHz|
|65 W|TDP|65W|

### Mainboard
Poah... Was soll ich über Mainboard schreiben? Es ist groß, als ATX Vollformat, ist schwarz und hat RGB-LEDs. Was aber jetzt der Unterschied zu B450/550 oder X570 ist vermag ich nicht zu sagen. Ich glaube, der Chipsatz ist schneller und dadurch läuft das System etwas fix aber Mangels Vergleichsoptionen kann ich es definitiv nicht sagen. Ansonsten die Üblichen Specs in der Tabelle unten.

||||
|--- |--- |--- |
|B450|Chipsatz|X570|
|--|PCIe 4.0 Lanes|24:
16x GPU
4x NVMe
4x Chipset Downlink|
|Alle Lanes über die CPU.
Insgesamt maximal 28 verfügbar|PCIe 3.0 Lanes|--|
|Nur Crossfire|Crossfire/SLI|Je nach Hersteller – Beides
möglich|
|Offizieller RAM-Takt: 2933
Mhz|RAM|Offizieller RAM-Takt: 3200 Mhz
(Je nach Hersteller bis
zu 5400 MHz)|
|Beides möglich, Übertakten
der CPU von
Spannungsversorgung des Boards abhängig|Übertakten von CPU und
RAM|Beides möglich|
|1x16|PCI-GPU Anbindung|1x16 / 2x8|
|2x USB 3.1 Gen 2;
6x USB 2.0;
8x SATA (Anzahl der SATA-Anschlüsse kann sich je nach
Hersteller ändern)|Anschlüsse|8x USB 3.2 Gen 2;
4x USB 2.0;
4x SATA (Anzahl der SATA-Anschlüsse kann sich je nach
Hersteller ändern)|
|ca. 5W|Chipset-Strombedarf|ca. 14W|
|Passiv|Kühlung|Meist: Aktiv mit Lüfter –
durch erhöhen Strombedarf
notwendig
X570S: Ohne aktiven Lüfter|
|mATX, ATX|Formfaktor|µATXm-ITX, mATX, ATX, eATX|

### Gehäuse
Wo ich was zu sagen kann, und das ist nicht viel gutes, ist das Gehäuse. Bisher war ich mit dem Fractal Design Define R6 ziemlich in der Luxuszone (Habs damals gekauft weil es das einzige Gehäuse ohne Fenster war). Es ist groß, es ist schwarz und sehr gut durchdacht. Nicht so beim Be quite!

Okay, etwas unfair ein Big-Tower mit einem Mid-Tower zu vergleichen aber ich machs trotzdem. Mir sind nämlich so einige Sachen aufgefallen die mich stören:

* Als Mid-Tower ist es prinzipbedingt ziemlich eng aber vor der Buchse für die CPU Stromversorgung noch einen fetten Lüfter zu verbauen sodass man mit den Händen nicht mehr ran kommt ist schon reichlich panne. Ich musste den Lüfter erst raus nehmen, Stecker reinstecken und anschließend wieder einbauen.

* Ich will jetzt die die Rosinen picken und der Kritikpunkt hier ist eigentlich zweitrangig aber auf der Seite wo das Mainboard verbaut wird ist eine Art Steg wo zwei 2.5 Zoll Laufwerke verbaut werden können. Soll wohl schick aussehen wenn eins der Seitenteile verglast ist. Was mich allerdings ein wenig stört ist, dass die Kopf an Kopf liegen (bzw. hängen). Also bei einer SSD kann lesen was drauf steht und bei der anderen muss man einen Kopfstand machen um es entziffern zu können.

* Der allergrößte Makel den das Gehäuse hat ist aber die Lautstärke. Wenn Grafikkarte, Gehäuselüfter und CPU-Lüfter auf Touren kommen wird es laut. Sehr laut. Und das für ein Gehäuse von einer Firma deren Anspruch es ist, Zubehör zu bauen das einen PC leiser machen soll. So sind zum Beispiel die Gehäuselüfter nicht entkoppelt und dröhnen am Gehäuse wie weiss ich nicht was. Die Dämmplatten sind auch eher Schein als Sein und lassen fast alle Geräusche durch.

## Fazit
Insgesamt ist das System durch eine schnellere CPU und ein andere Mainboard etwas fixer geworden, dafür aber auch etwas lauter weil das Gehäuse nicht so gut gedämmt ist. Außerdem muss ich nochmal an die Verkabelung, die gefällt mir noch nicht. Trotzdem muss ich sagen dass ich relativ zufrieden damit bin.