
# fantastische Schenkelklopfer
> 2. Januar 2021 von ragnar

Frohes Neues!

Tja…. 2020 ist vorbei. Viel wurde zu Silvester nicht gefeiert und noch viel weniger geknallert. Trotzdem haben wir für unsere Tochter ein paar Wunderkerzen und Tischfeuerwerk besorgt. Neben dem üblichen Plastik-Schnickschnack, glitzernde Sterne und Konfetti bzw. Luftschlangen waren im Tischfeuerwerk Zettelchen mit Witzen die ich euch nicht vorenthalten möchte.

Viel Spass!

Schlüpfrig

> „Papa, wenn du mir Geld gibst, dann erzähle ich dir, was der Postbote immer zu Mami sagt.“
„Hier sind 10 Euro. Also los!“
„Guten Morgen, Frau Ackermann, hier ist Ihre Post.“

Klaus wird Biologe

> „Klaus, kannst du mir sagen, wie lange Krokodile leben?“
„Ich nehme an, genauso wie kurze!“

Nicht hübsch aber selten

> „Ich möchte dieses Bild wieder zurückgeben. Es gefällt mir nicht mehr.“
„3. Stock. Badabteilung. Dort können sie den Spiegel umtauschen.“

Gute Frage

> Ein Mann läuft hinter einer Frau, die Frau dreht sich um und fragt: „Wieso laufen Sie mir die ganze Zeit hinterher“.
Antwortet der Mann: „Jetzt wo ich Sie sehe, frage ich mich das auch.“

ÖPNV

> Stehen 2 Streichhölzer im Wald, kommt ein Igel vorbei, sagt das eine Streichholz zum anderen: „Ich wusst ja gar net, dass heut ein Bus fährt“

Wer sich zuerst bewegt hat verloren!

> Fragt ein Beamter den anderen: „Wieso meckern die Leute eigentlich immerzu über uns, wir tun doch gar nichts?“

Dentologie

> Was ist rot und schlecht für die Zähne ein Backstein

