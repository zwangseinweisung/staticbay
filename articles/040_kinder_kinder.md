# Kinder, Kinder. Was wären wir nur ohne Kinder
> 9. Januar 2023 von ragnar

Als Vater von 2 Kindern wird es nie langweilig. Einer der beiden ist immer erkältet, während das andere Kind nur Blödsinn macht. Heute aber hat der Blödsinn dem Fass den Boden ausgeschlagen und ich ärgere mich grade über meine kleine Kröte (sie ist 1). 
 
Eins ihrer liebsten Hobbies ist es an meinem Desktoprechner rumzuspielen. Vor kurzem hat sie nämlich den Powerbutten entdeckt und fing an, mit wachsender Begeisterung, darauf rumzudrücken. Aber nicht einmal kurz, neeee… wenn schon Denn schon… in schneller Folger immer wieder darauf rumdrücken und Spass inne Backen dabeihaben. Vorläufiges end vom Lied, war erstmal, dass ich ein Reststück Plexiglas genommen habe und die Tasten sowie die vorderen USB-Anschlüsse abgeklebt habe. 
 
Naja… Lang hats nicht gehalten. Als ich heute Abend meinen Rechner einschalten wollte, fühlte sich der Powerbutton sehr seltsam an. Der war irgendwie verklemmt und ließ sich zu nichts überreden. Also habe ich flux den Schraubendreher ausgepackt, die Frontblende vom Gehäuse abgemacht und mir das Panel genauer angesehen. Was dabei rauskam war weniger schön. Mir purzelten sämtliche Plastik-Ersatzteile entgegen. Na Klasse! 
 
Um wenigstens arbeiten zu können wollte ich dann den Rechner am Pinheader (da wo der Powerbutton angeschlossen wird) einschalte aber Pustekuchen. Kein Bild, kein Ton, wir kommen schon! Hä?!? Ist irgendwas mit der Stromversorgung? Anderes Stromkabel probiert, nix! Funktioniert das Netzteil? Das funktioniert (das grüne Kabel und eines der schwarzen Kabel kurzschließen). Schlussfolgerung: Entweder hat das Mainboard einen Weg oder die CPU hat das ewige „Ein, Aus, Ein, Aus, Ein, Aus, usw. usf.“ nicht vertragen. Irgendwas am System ist über die Wupper gegangen und weil ich keine anderen Teile zum Testen habe, bleibt es bei der Diagnose. 
 
Toll! Jetzt darf ich mich um Ersatz kümmern. Und dass in Zeiten von Corona, Inflation und Lieferengpässen. Ich hoffe nur dass die Grafikkarte (eine Nvidia 3060 TI), der SSD und der Arbeitsspeicher überlebt haben. In dem Falle wird es richtig teuer 🙁 

Was ich auf jedenfall brauche, ist ein neues Mainboard, eine neue CPU (jeweils um die 180 Euro) und ein neues Gehäuse, weil der Powerbutton im Eimer ist. Als ich mir das Gehäuse gekauft habe (ein Fractal Design Define R6) gab es optional ein sogenanntes USB-C Panel. Das konnte man anstatt des normalen Panels einbauen. Leider gibt es das Panel nirgendswo mehr zu kaufen. Das wäre einfach und billig (um die 20 Euro) gewesen. Da ich am liebsten wieder ein Define Gehäuse hätte muss ich dann wohl ein neues kaufen. Nochmal 200 Euro 🙁 
 
In Summe, um die 600 Euro für einen kurzen Spaß am Rechner. Kinder. Es wird nie langweilig mit denen.
