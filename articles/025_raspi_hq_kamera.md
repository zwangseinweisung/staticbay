# Zeitraffer mit einem Raspberry Pi und der HQ Camera
> 2. Juni 2020 von ragnar

Zeitraffer… Mit dem Thema bin Ich immer noch nicht durch. Angefangen habe ich mit meiner alten EOS 450D aber aus Kostengründen musste es damals bei einer Selbstbaulösung bleiben. Zwischenzeitlich habe ich mir dann einen Fernauslöser gekauft weil ich mit der Selbstbaulösung doch nicht so zufrieden war. So konnte ich z.B. nicht ohne weiteres den Intervall zum auslösen festlegen. Letztes Jahr segnete dann noch meine gute, alte Canon das zeitliche in ging in Rente. Zwischenzeitlich konnte ich mir zwar eine neue Kamera leisten aber die hat keinen Anschluss für einen externen Auslöser, die Firmware kann das auch nicht und Magic Lantern o.ä. gibt es nicht. Also blieb mir nichts anderes übrig als wieder selber was zu bauen.

## Zeitraffer mit dem Raspi
Ich habe hier noch ein paar unbenutzte Raspberry Pis rumliegen und was liegt näher als einen von denen dafür benutzen? Gesagt, getan und schwuppdiwupp war eine USB-Webcam angeschlossen. Ein paar Minuten später, und ein wenig googeln, hatte ich eine Lösung laufen. Naja, was soll ich sagen? So richtig zufriedengestellt hat mich die Lösung auch nicht. Das Bild war zwar in Full-HD aber qualitativ nicht so der große Reißer! Ganz im Gegenteil, die Bilder wirken verwaschen und teilweise unscharf. Da drehte mir der Autofokus wohl eine lange Nase. 🙁

## Die Rettung
Also ging auf die Suche nach einer neuen Webcam. Ich vermute mal: Wegen Covid-19 und dem damit verbundenen Homeoffice waren aber alle guten Webcams entweder ausverkauft oder nicht bezahlbar. Glücklicherweise haute aber die Raspberry Pi Foundation zeitgleich ihre High Quality Camera raus. Schickes Teil! Der Sensor kann maximal 12.3 Megapixel, ist auf eine kleine Platine gelötet und kann auf Stative geschraubt werden. Der Oberknaller ist aber das auf die Kamera verschiedene Objektive geschraubt werden können (so wie bei einer DSLR z.B.). Verfügbar (und bezahlbar) sind ein 6mm Weitwinkel und ein 16mm Teleobjektiv.

Gesehen, nicht lange gefackelt und bestellt! Einige Tage später (Verdammter 1. Mai und das darauf folgenden Wochenende!) kam ein kleines Paket an. Ausgepackt, fliegend aufgebaut und getestet. Alter Schwede! Das Dingen ist gut! Aber es stellt sich schnell heraus dass das “Tele” kein wirkliches Teleobjektiv ist. Es hat eine feste Brennweite von eben 16mm. Ich stelle mir unter einem “Tele” ein Objektiv vor das einen Bereich (in dem Fall z.B. 6 bis 16mm) abdeckt. Ist aber nicht so, trotzdem macht es Spass damit zu knipsen.

## Hardware
Tjoa… Es macht wenig Sinn die Komponenten so auf dem Tisch liegen zu haben. Am liebsten habe ich alles schön klein und kompakt und habe vorher mir überlegt wie sich das am besten alles miteinander kombinieren lässt. Leider gibt es von der Raspberry Pi Foundation nichts fertiges gibt fing ich an zu basteln.

## Ich bau mir eine Kompaktkamera
Als Basis habe ich einen Raspberry Pi 4 (4GB) genommen und den zunächst in eins dieser Alugehäuse mit aktiven Lüftern verbaut. Alles Schön und gut, die Kamera lässt sich ja auf das Stativ schrauben aber der Raspi baumelte motivationslos am Flachbandkabel rum. Weil ich keine Lust auf Kabelbinder haben und ich das Gebilde auf mein Stativ schrauben wollte musste eine andere Lösung her. Also fing ich an das Alugehäuse wieder auseinander zu nehmen.

Ursprünglich wollte ich eine Art von Winkel bauen den ich zwischen die Verschraubung des Gehäuses bauen wollte. Ich fand aber nicht was ich hätte nehmen können. So blieb mir nichts anderes als die Kamera direkt auf das Gehäuse zu schrauben. Dazu habe ich mir eine Bohrschablone auf Papier aufgezeichnet (Der Abstand der Löcher beträgt 30mm) und die zu bohrenden Löcher auf die Innenseite des Gehäuses Übertragen (einfach mit einem Körner vorgekörnt) und anschließend die Löcher gebohrt. Weil ich nur Senkkopfschrauben hatte (M2.5) musste ich die Bohrlöcher noch etwas senken bis es passte. Danach mussten die Löcher noch entgratet werden und fertig war die Grobe Arbeit. Was noch blieb war die Kamera auf das Gehäuse schrauben (Abstandshalter nicht vergessen sonst liegen die Kontakte der Kameraplatine auf dem Alugehäuse), das Flachbandkabel reinfummeln und das Gehäuse zusammenschrauben. Fertig. Irgendwie sieht der Raspi mit der Kamera jetzt aus wie eine Kompaktknipse 😀

## Software
Die Hardware steht und jetzt brenn ich natürlich darauf das in produktiver Umgebung zu testen aber vorher muss der Raspi noch eingerichtet werden.

### Raspi vorbereiten
Ich werde hier jetzt nicht Anfangen vom A und Ω zu erzählen, das haben andere schon x-fach gemacht, einfach nach “raspberry pi einrichten” googlen und gut ist (z.B. von Heise -> https://www.heise.de/tipps-tricks/Raspberry-Pi-einrichten-so-klappt-s-4169757.html). Hier werde ich beschreiben was ich Softwareseitig geändert habe.

### Kamera einschalten
Zunächst muss die Unterstützung für die Kamera eingeschaltet werden. Das geht relativ simpel, einfach das Tool “raspi-config” starten, in den “Interfacing Options” die Kameraunterstützung einschalten. Die Kamera kann dann nach einem Neustart benutzt werden.

### NFS einrichten
> Egal wie groß eine SD-Karte ist, es ist nie genug Platz darauf!

Dementsprechend möchte ich das, hier zuhause, die Bilder auf meiner NAS landen. Dazu habe ich unter den Datei/Speicherdiensten ein Freigabe erstellt und im Netzwerk freigegeben. Da der Raspi seine IP über DHCP bezieht kann es sein das der bei jedem Neustart eine andere andere IP bekommt. Hier habe ich es mir einfach gemacht und die Freigabeberechtigung über das gesamte Netz gezogen (in meinem Fall 192.168.0.0/24).

NFS auf dem Raspi einrichten ist auch kein Hexenwerk. Einfach das Paket “nfs-common” installieren und in /etc/fstab die Freigabe eintragen. Feddich is der Lack.

### Alternative: USB-Stöckscken
Für Außeneinsätze, oder wenn man kein NFS Server zur Hand hat, tut es freilich auch ein ausreichend großer USB-Stick. Wie der formatiert/partitioniert ist und wo der gemountet wird bleibt jedem selbst überlassen. Ich habe z.B. einen 64GB großen USB3 Stick genommen und eine große EXT4 Partition drauf gemacht. Anschließend habe ich dann einen Ordner innerhalb von /mnt erstellt und lasse den Stick über die Datei /etc/fstab automatisch mounten.

## Fotografieren
Los gehts. Endlich kann ich mit dem Raspi fotografieren. Aber wie geht das nun?

### Normale Fotos machen

“Normale” Fotos machen ist kein großer Akt. Zum scharfstellen des Objektivs reicht das Tool “raspistill” einfach wie folgend starten und anhand des Livebildes die Blende und die Schärfe einstellen.

```bash
raspistill -t 0
```

Beenden lässt sich das Tool mit “strg+c”. Wenn alles eingestellt ist lässt sich mit

```bash
raspistill -o test.jpg
```
ein ganz normales Foto aufnehmen.

### Zeitraffen
Hier bin ich jetzt bei dem was ich eigentlich machen wollte. Timelapse. Raspistill ist in der Lage von sich aus Zeitrafferaufnahmen zu erstellen allerdings ist das nicht so komfortabel weil die Dauer der Aufnahme und der Intervall in Millisekunden angegeben werden muss. Jetzt könnte ich anfangen folgende Konstrukte zu bauen:

```bash
raspistill -tc $((30*1000) -tl $((2*1000)) -o timelapse-%04.jpg
```
In dem Beispiel oben wird 30 Sekunden lang, alle 2 Sekunden ein Foto gemacht. Wenig Intuitiv. Ich habe mich dann dazu entschieden die Aufnahmen per Cronjob laufen zu lassen. Das war für mich einfacher. Dazu habe ich mir ein kleines Bashscript mit folgendem Inhalt geschrieben:

```bash
#!/bin/bash
DATE=$(date +"%Y-%m-%d_%H%M")
raspistill -n -q 100 -ISO 100 -o /mnt/timelapse/timelapse-${DATE}.jpg
```

Das Script wird dann minütlich von cron gestartet und läuft dann, buchstäblich, bis in alle Ewigkeit. Das ist mir aber egal weil ich des Raspi ausschalte nachdem ich den benutzt habe, die Bilder sind ja auf der NAS bzw. dem USB Stick gespeichert.

### Video zusammentackern
Jetzt hab ich hier tausende Bilder und wie geht es nun weiter? Na zusammentackern natürlich! Hier bin ich erstmal der offiziellen Anleitung gefolgt. Das war so schön einfach. Auf dem Raspi selber wird dafür avconv benutzt aber es gibt mit mencoder, ffmpeg und wie sie alle heißen genug alternativen. Natürlich kann man sich vorher die Arbeit machen und einen automatischen Weißabgleich über alle Bilder machen (oder sonst wie bearbeiten) aber ich war neugierig wie das Ergebnis aussieht, also frisch ans Werk:

```bash
ls *.jpg > stills.txt
mencoder -nosound -ovc lavc -lavcopts vcodec=mpeg4:aspect=16/9:vbitrate=8000000 -vf scale=1920:1080 -o timelapse.avi -mf type=jpeg:fps=24 mf://@stills.txt
```

Ich finde, für einen kurzen Test, ohne sich näher mit weiteren Möglichkeiten zu befassen, ist das Ergebnis schon ganz ordentlich. Natürlich hat das alles noch verbesserungspotenzial aber ich bin erstmal damit zufrieden. Was will ich mehr 🙂

## Ausblick
Was kommt noch? Puh! Schwer zu sagen. Auf jeden fall bin ich mit der Situation unzufrieden dass das nur stationär funktioniert. Für Stopmotion mag das ja schon fast perfekt sein aber ich will Zeitraffer machen. Daher werde ich sicherlich noch ein kleines Display und einen mechanischen Auslöser irgendwie an den Raspi fummeln um den mobil nutzen zu können. Was mir allerdings Kopfschmerzen bereitet, der Raspberry Pi 4 ist sehr pingelig was die Stromversorgung angeht. Stabile 5.1V und mindestens 2.5A sollten es schon sein. Ich werd mir dann wohl mal eine geeignete Powerbank suchen müssen.

Außerdem wollte ich mich mit dem Thema Makrofotografie beschäftigen. Testweise habe ich mit dem 6mm Objektive mal einen BME280 Sensor fotografiert. Der hat eine Kantenlänge von 13,5×10,5mm. Wie man auf dem Bild sehen kann wird der aufgedruckte (oder gelaserte) Text aus kleinen, feinen Punkten zusammengesetzt. Schick! Gefällt mir, mit dem Thema werde ich mich noch mehr auseinandersetzen. Mir schwebt da schon was vor. Mal sehen wie gut sich das umsetzen lässt.

Was mich evtl noch reizen wird ist die stereoskopischen Fotografie. Im vergleich zu einer DSLR ist es bei dem Preis ist es kein Problem sich ein zweites Set anzuschaffen ein damit zu basteln. Das aber nur am Rande.

## Fazit
Die Kamera macht Spass, keine Frage. Sie macht super Bilder und die Steuerung über den Raspberry Pi ist mal was anderes. Hier liegt aber auch der größte Nachteil. Es ist nicht möglich die “mal eben” in den Rucksack zu schmeißen und loszuziehen. Andererseits, es ist ein Raspberry Pi! Da muss gebastelt werden sonst macht es ja keinen Spass.

Ich werd auf jedenfall Updates liefern.