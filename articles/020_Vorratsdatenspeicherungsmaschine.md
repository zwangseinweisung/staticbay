# 
# Ich bau mir eine Vorratsdatenspeicherungsmaschine
> 13. August 2019 von ragnar

Mein Fileserver zuhause ist vor einiger Zeit gestorben und jetzt musste ich mich um Nachschub kümmern. Allerdings gestaltete sich das etwas schwieriger als gedacht. Dieser Artikel trägt den Status „Work in Progress“. weil sich noch was am Server ändern wird. Daher dokumentiere ich hier was ich bisher alles 

## Wie fing alles an?
Um meine Daten zu sichern lies ich lange Zeit einen Rechner nebenher als „Fileserver“ laufen der meine Daten auf einer 1TB großen Platte speicherte. Das funktionierte so gut das ich mir keine Gedanken darüber gemacht habe und das ganz System hab laufen lassen (never touch a running system!). Leider hatte ich auch noch kein Monitoring am laufen und bekam nicht mit das die Datenplatte, auf der wirklich alle meine Daten lagen, im laufe der Zeit den Geist aufgab. Es kam, wie es kommen musste, beim nächsten Neustart klackerte die Platte fröhlich vor sich hin aber machte auch nix weiteres mehr. Scheiße! Alle Daten weg weil ich auch keine Backups angelegt hatte.

> Kein Backup? Kein Mitleid!

## Was kam dann?
Naja, irgendwie musste was Neues her. Ich hatte noch den Rechner über und fing daher an neue Platten zu kaufen und vorhandene alte Platten Wahl und planlos in das Gehäuse zu werfen um dann festzustellen: Irgendwie gefällt mir das nicht. Ich hatte zwar ausreichend Platz aber so richtig wohl fühlte ich mich nicht dabei. Ich kann es gar nicht richtig beschreiben, es war nicht das, was ich mir vorgestellt habe und bastelte immer mal wieder an dem Rechner rum, probierte Sachen aus und verwarf diese dann wieder. In der Zwischenzeit kam dann noch das echte Leben um die Ecke und forderte meine Zeit und so hatte ich das Thema nicht mehr ganz so dringend auf dem Schirm.

## Und nu?
Tjoa…. Vor Kurzem trat mein Chef an mich heran und meinte, dass die tägliche Datensicherung, die auf RDX-Medien geschrieben werden, nicht mehr das gelbe vom Ei sei und wir haben uns überlegt was wir stattdessen anstellen können. Eine Idee war es eine NAS dort aufzustellen und so kam das Thema in mir wieder hoch. Weil ich aber keine Ahnung von den Geräten habe (Immer noch nicht, dazu unten mehr) wollte ich mir privat eine besorgen und damit schon mal rumexperimentieren.  Vor allem wollte  ich wissen wie die Anbindung an ein bestehendes Active Directory funktioniert und was man sonst so mit den Dingern anstellen kann. Eben rumspielen 😉

## Auswahl der Hardware
Stromsparend ist das Schlagwort heutzutage aber das, was bei mir stehen soll, sollte auch mal’ne VM laufen lassen. Daher scheiden Consumer-NAS à laDS218 oder so aus. Die sind zu schmalbrüstig, als dass die VMs laufen lassen könnten. Größere Geräte oder gar welche aus dem Business-Bereich sind teuer bis fast unerschwinglich für „normalsterbliche“ (und schon gar nicht wenn man eine Familie hat) und wirklich stromsparend sind die dann auch nicht mehr. Das lange Gesicht, das ich zog, als ich die Preise für neue Geräte sah, wurde immer länger je mehr ich mich mit dem Thema beschäftigt habe. Oft kommen irgendwelche Geräte von QNAP, Synology und Co. raus die zwar ganz schick sind aber aus irgendeinem Grund immer wieder schnell vom Markt verschwinden. Das Ganze ging so weit das ich bereit war mir eine Synology zuzulegen. Ich hatte die schon auf meinem Amazon Wunschzettel gespeichert und als ich dann ein paar Wochen später die Flocken dafür zusammen hatte, stand das Ding bei etwa 1500 Euro (Statt 500). WTF! Warum?!? Sind das Anlageobjekte die ihren Wert steigern, weil die nicht mehr verfügbar sind?

Jedenfalls fiel mir jemand aus dem Chaosdorf ein. Vor einiger Zeit hatte ich ihm nach seinem Umzug geholfen Herd und Lampen anzuschließen. Da stand eine 6 oder 8 Port NAS rum, die er nicht mehr brauchte. Zu diesem Zeitpunkt brauchte ich die auch nicht und das Teil sammelte weiter Staub. Nachdem ich dann den Entschluss gefasst hatte mir eine zuzulegen und die Neupreise für mich unerschwinglich waren habe ich ihn gefragt was er dafür haben möchte. Nach einigen Wirren, er bot mir einen HP Microserver als NAS an aber ich meinte die „richtige“ NAS, wurden wir uns einig und ich holte denn den Microserver ab weil die „richtige“ NAS mittlerweile, an wen anders gegangen ist.

## Ans Eingemachte
Da stand der Microserver bei mir zuhause. So klein, so schwarz und ohne Platten. Naja, erst mal die Türe aufmachen und reinsehen. Viel gab es nicht zu sehen, denn die Wollmäuse hatten es sich darin gemütlich gemacht und wollten nicht ausziehen. Also fing ich an, bewaffnet mit Staubpinsel und Druckluft, die ersten unliebsamen Bewohner zu vertreiben. Nach der Aktion fing ich dann an alles auseinanderzunehmen und mit dem Staubpinsel alles Weitere zu bearbeiten. Was soll ich sagen, das Mainboard ist grün und nicht grau wie ich zuerst gedacht hatte.

### Reinfall #1
Nachdem ich dann wieder alles zusammengebaut habe, habe ich dann mal provisorisch ein paar Platten eingebaut und hab mir das bereits installierte FreeNAS angeschaut. Puh! Nicht nur innen war Staub, auch das FreeNAS war nicht mehr das Aktuellste. Zum glück ging der Download recht schnell aber hier zog ich erneut ein langes Gesicht. FreeNAS braucht mittlerweile 8GB Ram! Ich hatte aber nur 2 wovon einer defekt war. Glücklicherweise ist dem Rechner DDR3-Speicher verbaut sodass ich den Speicher aus meinem alten „Server“ erst mal recyceln konnte. Hier traf mich der nächste Blitz beim Kacken! Statt der verbauten 16GB Arbeitsspeicher wurden nur 8 erkannt. Himmel, Arsch und Zwirn! Egal! Hauptsache ich kann erst mal spielen.

### Reinfall #2
Ich installierte also FreeNAS und es war bunt, schick und die verbauten Testplatten ließen sich ganz simpel zu einem Raid verbinden. Samba für die Dateifreigaben war auch recht schnell eingerichtet aber dann ging es ins Detail. Wo ist denn die Benutzerverwaltung, wo kann ich Quotas einstellen, wie kann ich VMs auf der Kiste laufen lassen? Kurzum. FreeNAS war erst mal eine herbe Enttäuschung. Mag sein das FreeNAS viel kann und auch sehr gut ist aber die Lernkurve geht sehr schnell, sehr steil nach oben. Das konnte und wollte ich mir nicht antun, es gab genug andere Dinge zu erledigen.

### Reinfall #3
OpenMediaVault wird Oft als Alternative zu FreeNAS genannt. Okay…. dann schau ich mir das mal an. Ganz ehrlich gesagt, allem Anschein nach nullt OpenMediaVault beim anlegen des Raids die Platten zunächst oder synchronisiert da irgendwas rum und das dauert mir zu lange. Kurz mal bei Google nachgesehen was helfen könnte und in der tat wird man sehr schnell fündig. Viele Tipps laufen auf das Ziel hinaus die Syncronisationsrate über /proc/sys/dev/raid/speed_limit_min zu verändern aber das brachte leider nichts und außerdem hatte ich keine weitere Lust mich damit zu beschäftigen.

Tjoa… Wat nu? Nehmen wir doch mal das, was ich kenne. Windows Server. Schockschwerenot! Windows kann seit Windows 8.1 bzw. Server 2012 nicht mit der onboard Netzwerkkarte anfangen (dafür muss das Bios gemoddet werden). Kein Problem, ich hab ja noch einen freien PCI-E Steckplatz und eine Netzwerkkarte hier rumliegen. Also alles wieder auseinander getüftelt und die Netzwerkkarte eingebaut und anschließend wieder alles zusammen gepuzzelt. Aber jetzt drehte Windows mir ne lange Nase. Im Gegensatz zu FreeNAS, OpenMediaVault oder Linux alleine will sich Windows nicht auf dem USB -Stick installieren lassen. Hier musste dann also eine weitere Lösung her. Zum Glück hab ich hier noch eine 120GB SSD rumliegen (die sollte eigentlich mal an meinen Banana Pi M2 Berry. Aber wie es halt so ist, man kommt ja zu nix) und auf dem Mainboard war auch noch ein SATA-Anschluss (für ein DVD-Laufwerk) frei. Ne Stunde später war die SSD verbaut und Windows installierte sich ohne murren. Auch die verbaute Netzwerkkarte schickte die Pakete fehlerfrei auf reisen.

## Finale Konfiguration
Nachdem Windows Server jetzt läuft, die verbauten Platten zu zwei Raids zusammengefasst wurden und das Netzwerk funktionierte, fand ich, das ein DVD-Laufwerk doch noch zu ganz hilfreich wäre. Und wenn es nur zum rippen von Kinder Musik-CDs ist. Also, wieder alles zerlegen. Jetzt hatte ich ein kleines (Luxus)-Problemchen. Auf dem Mainboard ist neben der 4-Fach Mini-SAS Buchse nur ein SATA-Anschluss verbaut. Dort hängt jetzt aber die SSD für Windows dran. Zusätzlich sind noch 2 PCI Express Slots verfügbar, die aber auch beide belegt sind (mit der Netzwerkkarte und einer Grafikkarte). Weil aber auch ein VGA-Anschluss verfügbar ist konnte ich auf die Grafikkarte verzichten und stattdessen einen SATA Kontroller einbauen und daran das DVD-Laufwerk anschließen. Somit ist jetzt folende Hardware verbaut:

* 8GB RAM
* 4 * 4TB Western Digital Red
* 1 * 120GB SSD für Windows
* 1 * DVD Laufwerk

Als Betriebssystem kommt jetzt Windows Server Standard zum Einsatz. Lizenzen dafür gibt es auf verschiedenen Handelsplattformen für ein paar Euro. Und selbst wenn Microsoft die irgendwann blockieren sollte, ein Anruf bei Microsoft und man bekommt, zähneknirschend, eine neue Seriennummer (hab ich mit meiner anderen Lizenz schon durchgemacht).

Die 4 Platten sind jeweils zu zwei Raid 1 verbunden die sich nun gegenseitig synchronisieren. Im falle eines Ausfalls habe ich nun 2 Platten die sterben könnten ohne das es zu einem Datenverlust kommt.

## Backupkonzept
Ick hörse schon trapsen: „NAS sind kein Backup!“ und „RAID ist kein Backup!“. Recht hamse!

Die NAS dient bei mir in erster Linie als Datensammelbecken. Von dort aus wird auf eine weitere 4TB Platte (die extern per eSATA angeschlossen ist) das Backup geschoben. Zusätzlich werden die allerwichtigsten Daten noch auf verschiedene Clouddienste verteilt. So werden neue Fotos z.B. in meine Nextcloud geschoben oder nach Google kopiert. So komme ich hoffentlich nicht mehr in die Bredouille einen kompletten Datenverlust zu erleiden.

## Fazit
Ich habe zwar immer noch keine Ahnung von NAS & Co. weil ich mir wieder selber was zusammengefrickelt habe aber so fühle ich mich wohler! Das ganze System läuft rund, mit den Energiesparoptionen von Windows wird das ganze System dann auch relativ stromsparend und insgesamt ist es ist auch kein kuddelmuddel mehr. Okay, der Microsever ist nicht mehr der neuste aber das gibt mir wenigstens noch ein oder 2 Jahre um mich nach Ersatz umzusehen (tendiere wieder zu nem HP Microserver). Mal abwarten. In der zwischenzeit werden wir wohl auf der Arbeit eine fertige NAS kaufen und ich muss daran üben. Sicher ist aber das ich das ein oder andere mal an dem jetzigen System was verändern werde und dann hier dokumentiere. Stay tuned!