# Sneak Peak – Apache Webserver auf der FireBee
> 30. Oktober 2022 von ragnar

Ich wollte mal was ausprobieren und habe den Apache Webserver auf der FireBee installiert. Eigentlich kein großes Ding, der Webserver liegt schon seit ewigen Zeiten in den Sparemint Archiven vor aber es ist nicht mal eben „hingeworfen“. Also habe ich mich hingesetzt und Stichpunktartig meine Arbeit dokumentiert. Das werde ich noch ein wenig aufarbeiten müssen und werde die Dokumentation hier hochladen. Bis dahin, bleibt neugierig!