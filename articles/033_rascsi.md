# RaSCSI Images über NFS teilen
>6. Februar 2022 von ragnar

RaSCSI ist eine Hardware die einen Raspberry Pi um eine SCSI-Schnittstelle erweitert. Aber anders als z.B. SCSI2SD arbeitet RaSCSI nicht mit SD oder CF-Karten sondern mit Abbildern (Images). So ist es möglich dass mehrere SCSI-Geräte wie Festplatte, CD-ROM-Laufwerke oder einen Netzwerkadapter gleichzeitig emuliert werden können. Diese Images können auch von anderen benutzt werden sofern diese nicht verbunden sind.