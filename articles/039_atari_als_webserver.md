# Der Atari Computer als Webserver
> 30. Dezember 2022 von ragnar

Also, eigentlich wollte ich ja eine Artikel darüber schreiben, wie ich meine FireBee als Webserver eingerichtet habe. Aber ich merke, das wird zu Umfangreich. Daher hab ich mich dazu entschlossen den Artikel in 3 Teile aufzuspalten. Das gibt mir die Möglichkeit alles Detailierter zu beschreiben. Ich dachte mir dass ich die neuen Artikel wie folg aufbaue:

* Grundkonfiguration des Atari
* Einrichten des Apache Webservers auf dem Atari
* Einrichten des Apache Proxy-Webservers

Dazu kommen dann noch so notwendige Kleinigkeiten wie die Konfiguration des NAT und DyDNS damit der Webserver auch aus dem Internet erreichbar ist.

So schnell wird das aber nix, ist eher ein Projekt fürs nächste Jahr.

Also, bis dahin. Haltet die Ohren steif und schaut mal wieder rein.