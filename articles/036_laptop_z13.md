# Erfahrungsbericht Lenovo Thinkpad Z13
>30. Oktober 2022 von ragnar

Keine Ahnung, ob ich es bereits erwähnt habe, ich habe Rheuma und bin deswegen körperlich etwas eingeschränkt. Keine Panik, mir geht es gut und ich habe auch – noch – keine Deformationen an den Gelenken, aber so nach und nach baut mein Körper ab. War es bisher kein Problem für mich schwere Sachen zu schleppen oder den Rucksack bis Oberkante Reißverschluss vollzustopfen, so ist dieses so langsam nicht mehr möglich. Ich will nicht sagen, dass ich keine Kraft mehr habe, aber einiges fällt mir mittlerweile schwerer (im wahrsten Sinne des Wortes).

Mein bisheriges Arbeitsgerät war ein Lenovo ThinkPad P15 der ein echtes Tier ist. Groß, breit und kraftvoll. Leider ist der aber auch ziemlich schwer sodass es für mich echt mühselig wurde den jeden Tag zwischen der Arbeit und zuhause zu schleppen. Daher habe ich auf der Arbeit den Wunsch geäußert einen neuen Laptop zu bekommen. Von der Größe und vom Gewicht her sollte der in etwa so sein wie ein Macbook Air. Stellte sich heraus, dass das gar nicht so einfach war einen passenden Laptop zu finden. Es gibt zwar viele Geräte in der Größen- und Gewichtsklasse (Ultrabooks) aber entweder stimmt dann die Leistung nicht, oder irgendwas anderes war nicht gegeben.

Da ich hauptsächlich Linux nutze, lagen dann als erstes die Laptops von Tuxedo nahe [1], die wohl von Linux sehr gut unterstützt werden. Aber leider hatte mein Wunschlaptop so die ein oder andere Macke. Das Display war nicht Verwindungssteif oder, die angeblich gute Unterstützung war dann doch nicht so gut. Jedenfalls vielen die dann von der (Einkaufs)Liste.

Ich war auch bei Media Markt und hab mir die ausgestellten Geräte angesehen aber so richtig warm wurde ich mit denen nicht. Keine Ahnung, die waren mir nicht Business like genug. Billiges Plastik am Gehäuse, nur Full-HD, CPUs in der unteren Pentium-Klasse usw.

Nach einigem hin und her las ich dann bei Notebookcheck einen Testbericht über das ThinkPad Z13 und war sofort begeistert. Das war genau das, was ich wollte. Also schnell zu meinem Chef, dem das vorgestellt und dann beim Großhändler bestellt.

Jetzt sitze ich grade in der Bahn und schreiben diesen Artikel hier mit dem Z13 auf dem Schoß und es fühlt sich wunderbar an. So wunderbar, dass ich morgens auf dem Weg zur Arbeit 3-mal in den Rucksack geschaut hab, ob ich den Laptop dabeihabe. Der Rucksack war auf einmal so leicht, ich dachte ich hätte was vergessen 😀

## Hardware
Die genauen Daten könnt ihr bei Lenovo nachsehen, ich muss euch die hier nicht nochmal vorkauen. Die groben Specs sind aber:

* ein Ryzen Pro 7 6850U mit einer integrierten AMD Radeon 680M GPU
* 32GB Ram
* Zum draufgucken gibts noch ein 2880x1800px großes OLED-Display mit Touchscreen.

### Dimensionen, Gewicht und Verarbeitung
Das Z13 ist etwas kleiner als eine DIN-A4 Seite, und zusammengeklappt ungefähr so dick wie ein Zimmermannsbleistift. Bei der Größe und Ausstattung bringt der Laptop nur 1.2 Kilo auf die Waage.

Obwohl viel billiges Plastik, Aluminium und Glas verbaut wurde ist die Verarbeitung sehr gut. Das Gerät fühlt sich nach einem richtigen Business-Laptop an und nicht nach einem billigen Consumer-Plastikbomber. Nichts knarzt, oder biegt sich durch. Auch das Display ist, im Vergleich zum Tuxedo (todo: video einfügen) absolut stabil. Lenovo verwendet übrigens viele recycelte Materialien, sodass man den Laptop durchaus als nachhaltig bezeichnen kann (natürlich ist die Herstellung von elektronischen Geräten eine absolute Umweltsauerei, aber das ist ein anderes Thema).

Lenovo hat sich dafür entschieden die Kamera (1080p, 30 fps, f/2.0), die ohne mechanischen, Shutter kommt, in eine Art Buckel über dem Display zu verbauen. sieht ganz witzig aus, ein bisschen wie der Futureknick 😉

### CPU
Ich habe die CPU noch nicht wirklich geknechtet daher idelt die so um die 1Ghz rum was für Desktop, Browser, Email, Office und Co. locker reicht. Wenn es denn mal sein muss, kann die CPU bis auf 4,5Ghz hochtakten. Wie gesagt, habe ich bisher noch nicht machen müssen und bis jetzt ist der Laptop flüsterleise. Ich höre absolut nichts von den 2 verbauten Lüftern. Sicher, wenn der hochdreht, wird man die bestimmt hören aber so weit war ich noch nicht. Ich bin mal gespannt wie sich die Lüfter anstrengen müssen, sobald sich der erste Staub abgelagert hat.

### GPU & Display
Ich habe noch keine (3D) Benchmarks laufen lassen nur ein paar kleiner Spiele, die ich testweise installiert hatte. Half Life 2 oder Besiege laufen stabil in der 2K-Auflösung mit 60 FPS. Gut, beide haben ein Framecap und sind auch nicht mehr die jüngsten Spiele (das aktuellste, was ich habe, sind Frostpunk und Fallout 76, beide sind nicht „mal eben“ installiert) aber in 2K mit höchsten Grafikdetails, 16fach-Antialiasing usw. ist das schon nicht schlecht. Daher finde ich, kann das Z13 auch mal für die ein oder andere Spielerunde herhalten so lange die Spiele nicht zu anspruchsvoll sind.

Im Desktopbetrieb ist die GPU sehr flott und wird vom Linux AMD Treiber sofort gefunden, eingebunden und ist superfix. Ich weiß, glxgears ist kein Benchmark oder ernsthafte 3D-Anwendung, für einen schnelle Test reicht es aber. Auch hier erreicht das Z13 im Fullscreen Modus stabile 60 bis 65 FPS.

Das OLED-Display hat eine Auflösung von 2880×1800 und ist entspiegelt. Trotzdem sieht man noch die in oder andere Spiegelung, aber das ist nicht weiter dramatisch. Die Helligkeit ist auch in Ordnung, bei direkter Sonneneinstrahlung und dunklen Themes kann es manchmal schwierig werden was zu erkennen. Aber wer setzt sich schon mit dem Rücken zur Sonne 😉 Noch ein Punkt, von dem ich aber keine Ahnung habe und dementsprechend nichts sagen kann. Das Display ist wohl HDR fähig, keine Ahnung.

Was ich weniger, bis gar nicht, nutze, ist die Touchfunktion vom Display. Ich hab damit mal kurz rumgespielt und das wars dann auch schon. Über Gesten oder Zoomfunktionen kann ich nichts sagen. Touchdisplays an einem Laptop sind nicht meine Welt, sowas gehört meiner Meinung nach zu einem Handy oder Tablet oder meinetwegen ein Convertible Laptop.

Skalieren tu ich übrigens nicht, ich nutze die volle Auflösung bei 100% Skalierung, dementsprechend ist alles sehr klein und ich werde ganz oft von meinen Arbeitskollegen gefragt, wie man da was erkennen kann. Das kommt meiner Altersweitsichtigkeit, je weiter was weg ist (oder kleiner), umso besser kann ich was erkennen. Wenn was groß ist oder direkt vor der Nase dann verschwimmt alles.

### RAM
Wie oben beschrieben sind 32GB Ram verbaut. Soweit ich weiß sind die auch fest verlötet und lassen sich nicht tauschen. Angepriesen wird das Gerät mit Dual-Channel aber bei mir sind 4*8GB verbaut sodass ich davon ausgehen kann das hier ein Quad-Channel, wie von der CPU unterstützt, vorliegt.

### SDD
Pretty standard PCI-4 NVME SSD. Ziemlich fix. Der einzige „Pferdefuß“ ist, dass eine SSD verbaut wurde die eine halbe Baulänge (M.2-2242 ) hat. Die sind nicht einfach zu bekommen und wenn, doch dann sind die etwas teurer als „normale“ NVMEs.

### Tastatur, Touchpad und Trackpoint
Zunächst möchte ich feststellen, dass Tastaturen Geschmackssache sind, denn einige Testberichte sind der Meinung, dass die verbaute Tastatur nicht so gut ist. Ich mag sie aber, die Tasten lasen sich flott drücken, machen nicht so viel Krach und haben einen sehr kurzen Hub. Wie gesagt, für mich genau richtig.

Übrigens…. Lenovo wagt bei dem Z13 einen Versuch. Und zwar sind die linke STRG und FN-Taste vertauscht! Das kann man zwar im Bios wieder umdrehen aber ich mag das neue Layout.

Zum Touchpad kann ich nicht viel sagen, ich war noch nie ein Freund von den Dingern. Viel lieber habe ich den Trackpoint (egal ob Nippel oder Bauchnabel). Ich bin halt alt und woran man sich gewöhnt hat, will man nicht mehr hergeben. Wenn man die mechanischen Tasten vom ThinkPad kennt, muss sich ein bisschen umgewöhnen, denn die vom Trackpoint Tasten sind in der gleichen Fläche wie das Touchpad untergebracht und reagieren manchmal etwas träge, sodass man gelegentlich 2- oder 3-mal klicken muss. Das ist aber ein sehr subjektiver Eindruck, entweder erwischt mein dicker Wurstfingerdaumen die Tasten nicht sofort und richtig oder es ist wirklich die Hardware. Mir fehlt halt das passende Labor, um sowas rauszufinden. ¯\_(ツ)_/¯

### Dockingstation
Das ist ein Punkt, der mich am meisten nervt. Weil am Z13 nur 2 USB-C-Ports verbaut sind,ist man zwingend auf eine Dockingstation oder Portreplikator angewiesen, wenn der Laptop auf dem Schreibtisch liegt und als Desktopersatz herhalten muss. Ich habe einen ganzen Tag damit verbracht verschiedene Dockingstationen und Portreplikatoren auszuprobieren. Leider hat keine so richtig funktioniert. Entweder funktionieren die gar nicht (z.B. Dell WD-15) oder nur so halb. Die LenovoDocks die wir auf der Arbeit haben gaben z.B. USB und Netzwerk weiter, aber ich hatte auf meinen beiden Bildschirmen (Dell 2415Q, 4K) kein Bild. Weder über HDMI noch über DisplayPort. Das gleiche Spiel hatte ich auch mit einem Portreplikator, USB und Netzwerk ja aber nix auf dem Monitor. Was wohl funktioniert ist, dass, wenn der Laptop per USB-C an einen Monitor angeschlossen wird, dann kommt Bild, USB und Netzwerk am Monitor an, von wo aus ich dann weiterverteile.

> Update: Mit dem Kernel 6.0 unter Linux hat sich einiges verbessert. Die Unterstützung für Docks ist etwas besser geworden. Weil ich aber grade in Elternzeit bin und ich meinen Desktoprechner fest verkabelt habe kann ich grade nicht testen ob auch die Monitore funktionieren.

### Akku
Der 51W/h-Akku hält recht lange. Bisher hatte ich den Laptop aber stundenweise, so 2 oder 3 Stunden, vom Netz. Während mein alter Laptop dann schon knapp die Hälfte des Akkus aufgefressen hat, war der Akkustand des Z13 bei knapp 75%. Gut, das Z13 ist noch neu und mein P15 schon ein bisschen älter und verbrauchter, aber man merkt schon den Unterschied.

## Weiche Ware
Nun ist ein neuer Laptop nichts wert, wenn da kein Betriebssystem drauf ist. Da ich beruflich mit einer Windows-Domäne arbeite, brauch ich gelegentlich auch mal Windows. Klingt schlimmer als es ist, Windows 10 und 11 find ich gar nicht mal so schlecht, von daher kann ich damit leben, dass das auf der SSD rumgammelt.

Meine Vorliebe ist allerdings Linux, sodass ich neben Windows auch wieder ein Linux installiert habe. Tatsächlich kann ich, ich würde behaupten, 90% meiner täglichen Arbeit auch mit Linux erledigen (z.B. mit einem Webbrowser, Remmina für RDP oder dem Terminal). Für spezielle Spezialfälle habe ich dann noch zusätzlich die Powershell und den Webbrowser Edge von Microsoft installiert. Ja, ja… „Verbrennt den Hexer!“, ich weiß aber ich nutze die Tools, die am besten funktionieren und das ist nun mal Edge mit dem Office365 Admin Center oder die Powershell „wenns mal schnell“ oder wiederholbar sein soll.

### Windows
Windows wollte zuerst nicht so wie ich es wollte, ich habe die SSD in 2 Partitionen aufgeteilt und auf der ersten Partition wollte ich dann Windows installieren. Hat so Semi gut, nicht geklappt. Während der Installation ist Windows abgestürzt und ließ sich nicht mehr weiter installieren. Ich dachte: Ok, der Laptop ist ziemlich neu und mein USB-Stick, von dem ich Windows normalerweise installiere, ist recht alt. Also ein neues ISO-Image gezogen, auf den USB-Stick gepackt, davon gebootet und Windows dieses mal ohne Probleme installiert. Geht doch, warum nicht gleich so? 😉

Als es dann daran ging den Rechner in unsere lokale Windows Domäne zu hängen bin ich erstmal, ziemlich Anfängerhaft, auf die Nase gefallen. Anstatt die Pro Edition hat der Installer mir eine Windows Home Edition auf die SSD geballert. Das habe ich natürlich erst dann gemerkt als ich schon Linux installiert hatte. Zu allem Überfluss ist Windows der Meinung, dass mein Key, der im Bios gespeichert ist, kein gültiger Windows Pro Key sei. Wie dem auch sei, ich werde erstmal damit arbeiten und wenn ich in Elternzeit bin mich damit beschäftigen. Hauptsache ich bin irgendwie erstmal arbeitsfähig.

### Linux
Linux Mint Mate Edition von der Stange. Nix besonderes zu berichten darüber. Der Laptop und die verbaute Hardware wird ohne Probleme erkannt und eingerichtet. Alles ist so wie es sein sollte. Lediglich das Problem mit den Dockingstationen besteht aktuell noch. Immerhin kann ich USB und Netzwerk nutzen, wer braucht schon einen externen Monitor bei dem schönen Display? 😉

Das Einzige, worüber Linux sich beschwert hat, waren fehlende Firmwaredatein, die aber ziemlich schnell von Github runtergeladen und ins System kopiert sind. Danach ist Ruhe im Karton 🙂

Was mir hin und wieder sauer aufstößt: Linux bekommt es nicht geschissen das Default Audiodevice richtig einzustellen. Ständig muss ich nachhelfen und den richtigen Ausgang manuell einstellen. Klar, ich könnte Alsa in der Config sagen was das richtige wäre, aber automatisch wäre schon schöner.

## Samma, Summe zusammen
Ich bin eigentlich ganz Happy mit dem Gerät, die Hardware stimmt, und auch sonst gibt es nicht viel zu klagen. Nur das Problem mit der Dockingstation könnte gerne zügig behoben werden. Leider ist das Gerät noch so neu, sodass ich kaum andere Artikel im Netz dazu gefunden habe (also weder eine Produktvorstellung ala Notebookcheck, eine Ankündigung wie auf vielen Newsportalen etc.). Lediglich einen Post bei Reddit habe ich gefunden aber keinen Support oder Sonst was Artikel in der Richtung. Time will tell heißt es so schön. Ich werde das Gerät erstmal nutzen und immer mal wieder im Netz nach weiteren Erfahrungsberichten und Supporteinträgen durchforsten. Bin gespannt was da noch alles auftauchen wird.

### Pros
Insgesamt ein gutes Gerät. Hier eine kleine Aufzählung meiner Highlights:

* gut verarbeitest Gehäuse, Größe und Gewicht
* Im Vergleich zum meinem alten Laptop, sehr lange Akkulaufzeit
* Sehr gute Leistung des Systems. Mit genügend Reserven für zukünftige Anforderungen.
* Richtig gutes und helles 16:10 OLED-Display. Die Touchfunktion brauch ich nicht aber der eine oder andere wird sie mit Sicherheit lieben.
* Andere Testberichte sagen, dass die Tastatur gut, aber nicht die gewohnte ThinkPad Qualität hätte. Das mag sein, kann ich aber nicht bestätigen. Ich mag die Tastatur und auch das Schreibgefühl.

### Cons
Eigentlich gibts es, für mich, nicht so viel auszusetzen. Ist alles eher Nitpicking. Trotzdem zähle ich sie mal auf:

* Kein Showstopper aber schon ein reichlich großer Pferdefuß. Die Probleme die ich mit den Dockingstationen habe. Bei nur 2 Anschlüssen und einer davon fürs Powerdelivery, andere können auf gleicher Größe mehr Anschlüsse
* Die Soundprobleme unter Linux sind nervig. Ist mit Sicherheit nur eine Einstellungssache aber nicht unbedingt Je-Mensch will das dauerhaft machen. Daher für mich ein Con
* Das Touchpad und ich werden keine Freunde. der undefinierte Druckpunkt lässt mich ständig im Unklaren darüber, ob ich nun geklickt habe oder nicht.
* kein mechanischer Shutter vor der Kamera. Ist aber nix, was nicht ein Streifen Postit lösen könnte
