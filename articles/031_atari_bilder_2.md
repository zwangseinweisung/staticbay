
# Berühmte Atari Bilder – Teil II
> 3. August 2021 von ragnar

In dem Artikel „Berühmte Atari Bilder“ (https://theragnarbay.org/beruehmte-atari-bilder/) habe ich bereits die 747 und ein Space Shuttle (die Columbia) in SVGs konvertiert. Dabei habe ich den EPS Export von Kandinsky genutzt der so leidlich funktioniert. Bei den beiden Bildern oben war so weit alles in Ordnung, nur die Umrisse waren gestrichelt statt durchgezogen. Bei dem Tiger hier war es etwas anders. Der Exporter funktionierte insoweit, dass zwar die Grafik exportiert und von Inkscape gelesen werden konnte aber alle Flächen waren schwarz!

Naja, nun gut. Also fing ich an die Flächen auszufüllen. Problem hierbei war, dass Kandinsky eine andere Farbcodierung nutzt (0-999 statt wie heute bei RGB üblich 0-255). Deswegen habe ich mir ein Screenshot auf dem Atari erstellt und mit Hilfe von Gimp und der Farbpipette die Farben ausfindig gemacht. Das hat mich schon ein bisschen wahnsinnig gemacht denn der originale Autor des Bildes hat für Farbverläufe mehrere Ebene übereinandergestapelt.

Wie dem auch sei. Einige Stunde später hatte ich das Bild „ausgemalt“. Ich hoffe, es gefällt euch.

Ein bisschen was zum Copyright.Keine Ahnung ob und wenn ja welche Lizenz oder welches Copyright auf dem Bild liegt, von mir aus könnt ihr „mein“ SVG gerne kopieren. Ich hab es als CC NC-BY-SA (Namensnennung, nichtkommerzielle Verwendung, gleiche Bedingungen) deklariert.