# BananaPi – Datengrab
Basierend auf dem Banana Pi hab ich meinen alten Desktop der bisher als Datengrab gedient hat auch in Rente geschickt. Das schwierige daran war dieses mal ein Gehäuse zu finden das eine 3.5″ Platte, den BPi aufnehmen kann und eine Stromversorgung für beides bietet. Zuerst dachte ich an ein Gehäuse das zwei 5″ Geräte aufnehmen kann. Davon habe ich nämlich noch zwei Stück davon hier rumstehen (SCSI-Leergehäuse für meine Ataris) aber dann viel mir mein Streamer ein.

Dessen Gehäuse ist recht schick, es fasst normalerweise einen 5″ Streamer in voller Länge, ein eingebautes Netzteil mit +5V und +12V, Platz satt für ne Festplatte und den BPi.  Fluchs wurde das ausgeschlachtet (den Streamer kann ich evtl. mal irgendwann an einen Atari anklemmen und auch die SCSI-Anschlussleiste lässt sich bestimmt irgendwann mal recyclen) und mal probeweise alles reingelegt. Passt! Die 3.5″ Platte wird mit Metallwinkeln auf das Bodenblech geschraubt und so weit wie möglich nach vorne geschoben um hinten Platz für den BPi zu haben. Auch der Lüfter musste erstmal weichen. Der war viel zu dick und wir bald™ gegen einen schmaleren ausgetauscht. Das steht auf TODO-Liste unten.

Etwas kniffeliger war die Halterung für den BPi selber.  Dazu habe ich auf Abstandsbolzen mit M3 Gewinde zurück gegriffen die wir noch im Chaosdorf liegen hatten. Dazu habe ich mir einen schicken Platz im Gehäuse ausgesucht und den für das erste Bohrloch in das Gehäuse gelegt. Nachdem ich den Punkt hatte habe ich mit einem Messschieber mit die anderen Bohrlöcher angezeichnet und anschließend mit einer Standbohrmaschine gebohrt und gesenkt (ich hatte nur Senkkopfschrauben). Die Schrauben habe ich dann mit einer Unterlegscheibe und Muttern gesichert und anschließen dann die Abstandsbolzen darauf geschraubt. Damit der BPi auch ohne Spannungen passt mussten die Halter noch ein wenig hin und her gebogen werden. Zur Isolation, da ist schließlich viel Metall im Spiel, habe ich dann noch diese roten Hartpapierunterlegscheiben benutzt.

Zur Stromversorgung des BPi habe ich mir einen Molex auf SATA Adapter mit zwei Abgriffen besorgt.  Keine wirklich spannende Sache, einen SATA-Anschluss abgeschnitten, die nicht benötigten Adern mit Schrumpfschlauch isoliert, an die anderen Adern ein Micro-USB-Kabel angelötet und nochmal alles mit Schrumpfschlauch und Isoband isoliert. Bei Zeiten werde ich noch eine Schutzschaltung an den +5V Zweig hängen damit der BPi nicht durch ne Überspannung oder so gegrillt wird. Das steht auch noch auf meiner TODO-Liste. Was den Verbrauch angeht sind 12,8 Watt im Leerlauf und 13,7W unter Last sind schon ganz o.k. finde ich.

Das zusammenbauen war dann reine Formsache. Festplatte rein, BPi auf die Abstandsbolzen, alles verschrauben und verkabeln. So gut wie fertig. Ja Scheiße! beim platzieren habe ich das Netzwerkkabel und das USB Kabel vergessen! Also Netzwerkkabel genommen und vorsichtig den Knickschutz mit einem scharfen Messer abgehobelt und reingesteckt. Passt so leidlich. Schön ist das nicht aber es funktioniert. Packet drops oder ähnliches habe ich bisher nicht festellen können. Die USB-Buchse passte so ganz genau in das Loch wo man vorher die SCSI-ID einstellen konnte. Aber auch hier musste ich das Gehäuse vom Kabel entfernen damit das passt. Deckel drauf Fertig!

Als Betriebssystem habe ich mich dann wieder zu Bananian entschieden. Irgendwie lande ich immer wieder bei Debian :). Auch hier, nix spannendes. Image auf SD-Karte schreiben, booten, einrichten, loift! NFS war auch schnell installiert:

```bash
ragnar@datengrab:~$ sudo su
[sudo] password for ragnar:
root@datengrab /home/ragnar # apt-get install nfs-kernel-server nfs-common
root@datengrab /home/ragnar # mkdir /media/datengrab
root@datengrab /home/ragnar # mount /dev/sda1 /media/datengrab
root@datengrab /home/ragnar # chown nobody:nogroup /media/datengrab
root@datengrab /home/ragnar # echo -e "/media/datengrab/\t192.168.1.0/24(rw,sync,no_subtree_check)" &gt;&gt; /etc/exports
root@datengrab /home/ragnar # /etc/init.d/nfs-kernel-server restart
```
Als erstes wollte ich natürlich testen wie schnell der BPi auf die Platte schreiben kann. Dazu habe ich erstmal ein paar Tests laufen lassen: (zum Vergleich auch auf eine SD-Karte)

```bash
ragnar@bananapi:~$ time sh -c "dd if=/dev/zero of=ddfile bs=8k count=100000 &amp;&amp; sync"
100000+0 Datensätze ein
100000+0 Datensätze aus
819200000 Bytes (819 MB) kopiert, 57,9641 s, 14,1 MB/s
 
real 1m6.562s
user 0m0.220s
sys 0m6.830s
```
Messwerte der OCZ Agility 3 (64GB SSD) auf Host MPD

```bash
ragnar@mpd:~$ time sh -c "dd if=/dev/zero of=ddfile bs=8k count=100000 &amp;&amp; sync"
100000+0 Datensätze ein
100000+0 Datensätze aus
819200000 Bytes (819 MB) kopiert, 16,6716 s, 49,1 MB/s
 
real 0m19.247s
user 0m0.090s
sys 0m6.640s
```
Messwerte der Hitachi HDS72101 (1TB HDD)

```bash
ragnar@datengrab:~$ time sh -c "dd if=/dev/zero of=ddfile bs=8k count=100000 &amp;&amp; sync"
100000+0 Datensätze ein
100000+0 Datensätze aus
819200000 Bytes (819 MB) kopiert, 39,6736 s, 20,6 MB/s
 
real 0m44.368s
user 0m0.160s
sys 0m8.670s
```
Wie zu erwarten bricht der keine Geschwindigkeitsrekorde aber für so ein kleines Teil sind 49 MB/s auf eine SSD schon ganz ordentlich. Ich finde aber auch das 20 MB/s für eine Platte in Ordnung geht die noch verschlüsselt ist.

Die erwähnte TODO-Liste umfasst noch ein paar Kleinigkeiten. So würde ich ganz gerne mit einem Taster von vorne ein und ausschalten. Dazu gehört dann noch eine Betriebs-LED und Festplatten-LED. Außerdem verbau ich in allernächster Zukunft einen DS1820 Temperatursensor den ich schon bei meinem BeagleWeather verwendet habe um den, noch nicht vorhandenen, Lüfter zu steuern.

Wenn ich ganz lustig bin verbau ich in die Frontblende noch ein LC-Display das die aktuelle Last, Temperatur und Plattenbelegung anzeigt. Mal sehen.

