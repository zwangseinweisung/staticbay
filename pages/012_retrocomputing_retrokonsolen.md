# Retroconsolen
Neben Atari Computer „sammel“ ich auch noch Videospielkonsolen. Bisher sind es nur 6 Stück die ich abwechselnd auch mal mit zu den Hinterhofzockern nehme. Hier eine Aufstellung der Geräte:

## Atari 2600 Jr (2. Generation)

Quasi der Großvater unter den Telespielen. Die Kiste ist stinknormal, kein S-Video Mod oder sonstige Dinge. Einfach nur ein 2600. Die paar Spiele die ich hab sind:

* Dschungel Boy
* Pacman Ms.
* Pacman
* Autorennen
* Fussball

Pacman und Ms. Pacman braucht man nicht weiter erklären.

Dschungel Boy ist ein Pitfall Clone der recht einfach gestrickt ist. Es fehlen z.b. die Alligatoren die hin und wieder mal ihr Maul aufreißen. Das Spiel hat aber auch einen „lustigen“ Bug. Es es nicht möglich über den Hund bzw. Skorpion zu springen. Wenn man es versucht stirbt man.

Autorennen und Fussball sind ebenfalls Clones, vermutlich von Quelle oder Hertie Anfang der 80er, von Grand Prix und International Soccer.

### TV Games
Ein keines Bonbon ist der „TV Games“ was ich habe. Das ist im Grunde genommen ist das ein vollständiger 2600 der Batteriebetrieben wird. Einfach Batterien rein, Kabel an die Glotze und losspielen. Vorinstalliert und nicht austauschbar sind folgen Spiele:

* Asteroids
* Adventure
* Missile Command
* Centipede
* Gravitar
* Yar’s Revenge
* Breakout
* Pong
* Circus Atari
* Real Sports Volleyball

## Saba Videoplay (2. Generation)
Jetzt wird es richtig abgefahren. Das Saba Videoplay ist ein Lizenznachbau des Fairchild Channel F, eine 8-Bit Konsole von 1976. Fangen wir mal von hinten an. Was haben die XBOX oben und der Channel F gemeinsam? Antwort: Robert Noyce. Eben dieser entwickelte die Fairchild F8 CPU die in dieser Konsole werkelt. Noyce war es aber auch der die Firma Intel gründete (zusammen mit Gordon Moore – genau, der mit dem Moore’schen Gesetz) dessen CPU heute in der XBOX werkelt. Außerdem war das Channel F die erste Konsole die austauschbare Module hatte.

Auch sonst ging man andere Wege. Die fest verdrahteten Controller z.B. sind nicht nur 4-Wege-Controller sondern man kann den Hebel auch noch rauf ziehen bzw. runter drücken und drehen! Die Steuerung kann einen schon bekloppt machen! Aber das ist Teil des Spielspaßes. So kann man bei Fussball nicht nur den Feldspieler steuern sondern auch gleichzeitig noch den Torwart. Das Gerät hat bereits 4 eingebaute Spiele und kann mit weiteren Modulen gefüttert werden. Spiele die ich habe (weil die Durchnummeriert sind mal in anderer Aufzählungsweise):

* Videocart 1: Mühle/Tontauben-Schießen/Kreatives Malspiel/Videoscope
* Videocart 4: Luftkampf
* Videocart 5: Kampf im Weltraum
* Videocart 9: Autorennen (nach Fairchild egtl. Backgammon mein Modul ist von Nordmende)
* Videocart 10: Baseball
* Videocart 14: Schiffe versenken (nach Fairchild egtl. Völkerball mein Modul ist von Nordmende)
* Videocart 18: Bowling

## Sega MegaDrive 2 (4. Generation, Wikipedia Artikel)
Die Konsole ist ein Wenig gemoddet. Einerseits hat die einen, schon fast gewöhnlichen, 50/60Hz-Umschalter aber andererseits läuft der MC68000 mit 10MHz was eher ungewöhnlich ist. Overclocking bei Konsolen gibt es halt nicht so oft. Spiele dazu sind „nur“:

* Sonic the Hedgehog I
* Sonic the Hedgehog II

# Atari Jaguar (5. Generation, Wikipedia Artikel)
Eines meiner Schätze ist der Atari Jaguar den ich samt CD-Lauftwerk von einem Freund bekommen habe. Auch der Jaguar ist nicht gemoddet. Alles original. Spiele die ich dazu habe:

* Tempest 2000
* Cybermorph
* Atari Karts
* Ultra Vortek
* Defender 2000
* Super Burnout
* Trevor McFur in the Crescent Galaxy
* Kasumi Ninja
* Power Drive Rally
* Rayman
* Vid Grid (CD)
* Myst (Demo, CD)
* Battle Morph(CD)
* Blue Lightning(CD)
* Hover Strike(CD)

Außerdem, zum speichern der Spielstände ein so genanntes „Memory Track“. Das ist ein Flashspeicher der in den Modulport der Konsole gesteckt wird.

## Sony Playstation 2 (6. Generation)
Nix wildes, ne PS2 halt die an fast jeder Ecke zu finden ist. Spiele dazu sind:

* Rayman Revolution
* Rayman 3 – Hoodlum Havoc
* Grand Theft Auto – Vice City
* Tony Hawk’s Pro Skater 4
* Grand Turismo Concept
* Der Herr der Ringe – Die Rückkehr des Königs
* Medal of Honor – Frontline
* Medal of Honor – Rising Sun

## Microsoft XBOX (6. Generation)
Auch nix Weltbewegendes. Auch keine Mods oder umbauten. Spiele sind:

* Grand Theft Auto – San Andreas
* The Italian Job
* Tony Hawk’s Pro Skater 4
* Psychonauts
* Splinter Cell – Pandora Tomorrow
* Fifa 05
* Fifa 07