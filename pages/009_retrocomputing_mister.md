# MiSTer – Einleitung

> Platzhalter - Inhaltlich steht hier noch nicht viel, die Seite wird so nach und nach befüllt und dient derzeit als Platzhalter. Tu also noch ma ’n bisken rumgondeln, dat dauert noch wat. Kuck se bald ma hin un wieda rein

## Was ist MiSTer?
### Kurzgesagt:

MiSTer ist der indirekte Nachfolger vom MiST. MiST wiederum bedeutet AMiga & Atari ST. Damit ist dann auch schon fast alles gesagt 😉

### Langgesagt:
Oben schrieb ich ja dass das MiSTer Projekt ein indirekter Nachfolger des MiST ist. Das liegt daran dass der verbaute Cyclone III mit seinen 25 tausend Logikeinheiten zu klein geworden ist um aufwendigere Systeme umzusetzen. Daher wurde als Nachfolger auf das Terasic DE10-nano Board gesetzt. Der verbaute FPGA ist mit 110 tausend Logikeinheiten etwas über vier mal so groß, es sind bereits 1GB DDR3 Arbeitsspeicher verbaut und der ARM Cortex A9 läuft mit 800Mhz was ihr für weitere Einsatzgebiete prädestiniert. Der ARM übernimmt die Verwaltung der Floppy bzw. HD-Images, kümmert sich um die SD-Karten oder überwacht die IO-Ports. Außerdem läuft auf dem ARM noch ein sehr reduziertes Linux zur Systemverwaltung. Hinzu kommen ein HDMI Ausgang, USB-OTG, serial over USB, ein paar Schalter/Taster und LEDs zum Testen sowie jede Menge IO-Ports die, teilweise, sogar Arduino kompatibel sind.

### Was ist ein FPGA?
Genau wie alle anderen Chips bzw. ICs auch besteht ein FPGA zunächst aus einer Ansammlung von Transistoren, Logikelementen usw. die in einem Gehäuse verpackt sind. Der Unterschied besteht jetzt darin das die Logikelementen nicht permanent, wie bei der CPU, fest miteinander verbunden sind sondern sich nach belieben verbinden lassen.

Die FPGA-Emulation unterscheidet sich daher stark von der herkömmlichen Emulation auf der CPU. Eine herkömmliche CPU arbeitet die Befehle seriell ab während im FPGA alles parallel funktioniert wie in den originalen Geräten. Damit kommt FPGA der originalen Hardware ziemlich nahe.

Ich stelle mir das so vor: Der Chip 7408 (http://www.farnell.com/datasheets/59359.pdf) ist ein 4-Fach Und-Gatter, der am Ausgang (z.B. Pin3) ein Signal liefert wenn an Pin 1 und 2 ein Signal anliegt. Jetzt kann ich hingehen und diese Schaltung auf einem Steckbrett nachbauen (schematische Darstellung rechts), was mit 2 Tastern, einer Batterie und einer Lampe schnell gemacht ist. Die leuchtet nur dann an wenn die  Taster E1 und E2 gedrückt werden.

Das war jetzt ein sehr, also wirklich, sehr einfache Beispiel. FPGAs sind in der Lage hochkomplexe, digitale Schaltungen nachzubilden. Wer mehr wissen möchte kann sich den passenden Artiekl dazu auf der Wikipedia (https://de.wikipedia.org/wiki/Field_Programmable_Gate_Array)durchlesen oder sich das Video von EEVblog (https://www.youtube.com/watch?v=gUsHwi4M4xE) ansehen.

### Was ist (V)HDL?
Übertragen wir das jetzt das Steckbrett auf den FPGA bedeutet das jetzt dass eine spezielle HDL-Sprache beschreibt, wie alle diese Taster und Lampen miteinander verbunden werden. Dazu werden Ein aud Ausgänge definiert und ein bisschen Magie betrieben. Für mich als „Nichtprogrammierer“ ist das schwer nachzuvollziehen aber das Internet ist  voll von Tutorials und Anleitungen falls es dich interessieren sollte.

## Addon-Boards
Für das DE10-nano Board gibt es ein paar wichtige Erweiterungen die ich hier kurz erwähnen möchte

### SD-Ram
Eine der wichtigsten Erweiterungen denn einige der Cores funktionieren nur dann wenn das Modul vorhanden ist. Auch hinsichtlich der verfügbaren Speichergrößen und Bauformen gibt es Unterschiede:

||Vorteile|Nachteile|
|--- |--- |--- |
|Vertikal|Vergrößert die horizontalen Abmessungen nicht. Deckt nicht ab und ermöglicht eine bessere Kühlungdes FPGAs. Blockiert nicht die Arduino GPIO - kompatibel mit zukünftigen oder benutzerdefinierten Erweiterungen. Einfach anzubringen / abzunehmen.|Etwas weniger maximale Arbeitsfrequenz.|
|Horizontal nach außen|Deckt nicht ab und ermöglicht eine bessere Kühlung des FPGAs. Blockiert nicht die Arduino GPIO - kompatibel mit zukünftigen oder benutzerdefinierten Erweiterungen. Höhere maximale Arbeitsfrequenz.|Vergrößert die horizontalen Abmessungen.|
|Horizontal nach innen|Vergrößert weder horizontale noch vertikale Abmessungen. Höhere maximale Arbeitsfrequenz.|Blockiert die Arduino-GPIOs - nicht kompatibel mit zukünftigen oder benutzerdefinierten Erweiterungen (PCB v3.2 behebt dieses Problem). Deckt den FPGA ab und erschwert so die Kühlung. Der Temperaturzustand ist ziemlich schlecht Nicht kompatibel mit I / O Board v4 und neuer!|

Das Ursprüngliche SD-Ram Board hatte eine größe von 32 Megabyte was für die meisten Cores völlig ausreichend ist. Einzig der Neo Geo Core ist hier die Ausßname. Die Roms sind schon so groß dass der Ram nicht mehr ausreichend ist. Daher werden mittlerweile auch Module mit bis zu 128MB angeboten.

### IO-Board
Das MiSTer IO-Board ist eine optionale Erweiterung. Es werden die folgenden Funktionen zur MiSTer-Plattform hinzugefügt:

* VGA-Anschluss
* 3,5-mm-Audioanschluss mit TOSLink
* 3 Tasten (OSD Menü, Reset und frei konfigurierbar)
* 3 Status-LEDs (Power, sowas wie eine HDD-LED und Benutzerdefiniert
* zweiter SD-Kartenslot für einige Cores
* Lüfter zur Kühlung des FPGA
* Erweiterungsanschluss (in Form eines USB 3.0-Anschlusses, ist aber kein USB!)
* Zusätzliche Anschlüsse zur Einbindung des MiSTer in ein Gehäuse

### USB-Hub
Dies ist eine optionale USB-Hub-Zusatzplatine für den bequemen Anschluss von USB-Geräten. Mit diesem Board verfügt der MiSTer über zusätzliche sieben USB-Anschlüsse. Der Hub basiert auf dem FE2.1-Chip, der nur wenige externe Komponenten benötigt. Es werden 3 Seiten für USB-Buchsen verwendet.

## Laufen lassen
Im grunde genommen kann man das Board auspacken, anschließen und einschalten. Alles was benötigt wird, wird mitgelifert. Auf der mitgelieferten SD-Karte (bei mir waren es  8GB) ist die Ångström Distribution vorinstalliert. Diese ist bereits altbekannt und läuft auch z.B. auf dem Beaglbone Black. Zusätzlich startet noch ein LXDE-Desktop sodass man direkt loslegen kann. Allerdings gibt es ein paar Fallstricke zu beachten die ich hier dokumentieren möchte.

### Jumpstart
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

### Cores „Installieren“
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

### Aktuell halten
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

## Verschiedene Cores
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

### Atari ST
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

### ao486
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

### MageDrive
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.


### memtest
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.