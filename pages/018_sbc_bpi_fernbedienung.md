# BananaPi – Fernbedienung
Da der Banana Pi ja einen Infrarotempänger mitbringt will man den früher oder später auch nutzen. Weil aber Lirc nicht ganz trivial ist hier ein kleines HOWTO. Ich benutze hier Debian (mit Kernel 3.4.104+), andere Linuxe sollten analog dazu funktionieren. Bei älteren Kernel heißt das IT-Modul anders, da gehe ich nicht drauf ein.

Zunächst wird sicher gestellt das der Kernel auch die Mindestanforderung erfüllt.

```bash
ragnar@bananapi:~$ uname -a
Linux bananapi 3.4.104+ #3 SMP PREEMPT Wed Nov 19 08:28:34 CET 2014 armv7l GNU/Linux
```

Anschließend wird erstmal lirc selber und ein paar tools installiert:

```bash
sudo apt-get install lirc evtest wget
```

Nachdem das getan ist wird das Kernelmodul zunächst von Hand geladen und anschließend in die Datei /etc/modules eingetragen.

```bash
sudo modprobe sunxi_ir
sudo echo -e "sunxi_ir\n" >> /etc/modules
```

Das wars so weit mit den Vorraussetzungen. Damit lirc später weiss welches Inputdevice benutzt wird lassen wir uns den Inhalt der Datei /proc/bus/input/devices ausgeben:

```bash
ragnar@bananapi:/dev$ cat /proc/bus/input/devices
I: Bus=0019 Vendor=0001 Product=0001 Version=0100
N: Name="sunxi-ir"
P: Phys=RemoteIR/input1
S: Sysfs=/devices/virtual/input/input0
U: Uniq=
H: Handlers=sysrq rfkill kbd event0
B: PROP=0
B: EV=3
B: KEY=ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe
I: Bus=0019 Vendor=0001 Product=0001 Version=0100
N: Name="axp20-supplyer"
P: Phys=m1kbd/input2
S: Sysfs=/devices/platform/sunxi-i2c.0/i2c-0/0-0034/axp20-supplyer.28/input/input1
U: Uniq=
H: Handlers=kbd event1
B: PROP=0
B: EV=7
B: KEY=100000 0 0 0
B: REL=0
```

Wichtig ist hierbei die Zeile H: Handlers=sysrq rfkill kbd event0. In diesen Beispiel sagt uns das der Eventhandler 0 dafür zuständig ist der das Inputdevice /dev/input/event0 benutzt. Um das zu prüfen benutzen wir evtest:

```bash
sudo evtest /dev/input/event0
```

Im besten fall sollte evtest erfolgreich melden wenn eine Taste auf der Fernbedienung gedrückt (Value 1) und wieder losgelassen (Value 0) wurde, welche das war (KEY_9 bzw. KEY_10) und den Keycode (code 10 bzw. code 11).

```bash
Testing ... (interrupt to exit)
Event: time 1418846929.868420, type 1 (EV_KEY), code 10 (KEY_9), value 1
Event: time 1418846929.868431, -------------- EV_SYN ------------
Event: time 1418846930.245826, type 1 (EV_KEY), code 10 (KEY_9), value 0
Event: time 1418846930.245835, -------------- EV_SYN ------------
Event: time 1418846931.787506, type 1 (EV_KEY), code 11 (KEY_0), value 1
Event: time 1418846931.787516, -------------- EV_SYN ------------
Event: time 1418846932.165849, type 1 (EV_KEY), code 11 (KEY_0), value 0
Event: time 1418846932.165858, -------------- EV_SYN ------------
```

Jetzt geht es ans eingemachte. Jetzt wird lirc selber eingerichtet. Dazu wird die Datei /etc/lirc/hardware.conf mit den Werten die wir vorhin rausgefunden haben (Kernelmodul und Inputdevice) geändert:

```bash
# /etc/lirc/hardware.conf
#
# Arguments which will be used when launching lircd
LIRCD_ARGS=""
 
#Don't start lircmd even if there seems to be a good config file
#START_LIRCMD=false
 
#Don't start irexec, even if a good config file seems to exist.
#START_IREXEC=false
 
#Try to load appropriate kernel modules
LOAD_MODULES=true
 
# Run "lircd --driver=help" for a list of supported drivers.
DRIVER="devinput"
 
# usually /dev/lirc0 is the correct setting for systems using udev
DEVICE="/dev/input/event0"
MODULES="sunxi_ir"
 
# Default configuration files for your hardware if any
LIRCD_CONF=""
LIRCMD_CONF=""
```

Nun trainieren wir lirc. Klingt komisch, ist aber so. Manchmal klemmt es irgendwo aber hin und wieder muss man zwei mal kurz die selbe Taste drücken oder lange gedrückt halten.