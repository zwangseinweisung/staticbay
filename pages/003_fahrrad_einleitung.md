# Fahrräder

Neben Computer ist Fahrrad fahren eine meiner Hobbies und Leidenschaft. Weil ich auch keinen Führerschein, und dementsprechend auch keine Auto, habe ist das Fahrrad das Beförderungsmittel der Wahl für mich. Daher ist es für mich auch ein Stück weit zum Politikum geworden. Deswegen bin ich auch Mitglied beim ADFC und engagiere mich bei der Critical Mass.

> Bike endorphin<br>so many times felt too tired to ride but rode anyway and never regret

Hier werde ich demnächst meine Räder zeigen und euch so zeigen was hier in Düsseldorf so gutes und schlechtes läuft.
