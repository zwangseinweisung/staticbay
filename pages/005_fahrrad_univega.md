# Crossrad – Univega Terreno 900
Mein „Langstreckenfahrzeug“ und Sattelschlepper an dem ich meinen Anhänger dran hänge. Ein klassisches Crossrad mit dem ich Strecken von >100km fahre. Das wurde von mir ein wenig umgebaut. Zum einen habe ich eine Anhängerkupplung angeschraubt und weil ich gerne hoch uns sehr gestreckt sitze, habe ich einen viel längeren (140mm) Vorbau verbaut. Ansonsten sind wegen des Komforts noch ein paar Steckschutzbleche dran gekommen.

Außerdem habe ich noch ein USB-Ladegerät verbaut das seinen Strom aus dem Nabendynamo erhält und zum Laden des Handys benutzt wird.

* Baujahr: 2013
* Rahmen: Alu, 55cm (gemessen von mitte Trelager bis Ende Sattelrohr)
* Baugruppe: Shimano XT
* Bremsanlage: Magura MT2 (Scheibe, Hydraulisch)
* Felgen: Mavic 622 x 19 (Vorderrad mit Shimano Alfine Nabendynamo)
* Beleuchtung:
    * vorne; Axa Pico 30,
    * hinten; Busch & Müller Toplight view
* Mäntel: Schwalbe Marathon 37×622
* Sattel: Brooks B17 Flyer