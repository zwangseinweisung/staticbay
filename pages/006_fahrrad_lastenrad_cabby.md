# Lastenrad – Gazelle Cabby
Seit dem sich unsere Familie um 1 vergrößert hat brauchen wir einen fahrbaren Untersatz für „mit Kind“. Fündig geworden sind wir in Viersen und haben uns ein Gazelle Cabby Lastenrad, für erstaunlich wenig Geld, geschnappt. Als Zubehör gab es noch verschiedene Regenverdecks und eine Halterung für MaxiCosi-Sitze dabei. Wie jedes Gebrauchtrad hat auch dieses ein paar Mängel. Die Mäntel sind runter und der Frontscheinwerfer ist eher eine Kerze im Marmeladenglas. Ansonsten ist das für ein 6 Jahre altes Rad noch erstaunlich gut. Kaum Rost oder andere, gravierende Mängel.

Demnächst werde ich hier sicherlich den ein oder anderen Bericht zu dem Rad abliefern.

> [ragemode]
Jedes Mal wenn ich das Rad mit dem Speichenschloss abschließen will ist eine dumme, verfi**te Drecksspeiche im Weg! Warum? Hab ich da ein besonderes Talent für die Dinger zu treffen? Jedes besch*ssene mal! Immer und immer wieder!
[/ragemode]



## [Update 1]
Die oben angesprochene Beleuchtung wurde ersetzt. Ursprünglich verbaute Gazelle eine Halogenlampe ein Batteriebetriebenes Rücklicht von Move. 2010 war das noch in Ordnung aber 2016 will man ein gutes LED-Licht haben. Entschieden haben wir uns daher für einen Scheinwerfer von Busch und Müller. Den Lumotec IQ2 Eyc. Eine technische Beschreibung spar ich mir jetzt hier, schließlich hat das bereits „Fahrradbeleuchtung Info“ (http://fahrradbeleuchtung-info.de/testbericht-busch-mueller-lumotec-iq2-eyc) getan. Nur so viel. Die „bemängeln“ das der Scheinwerfer den Nahbereich sehr hell ausleuchtet, den Fernbereich aber nicht so dolle. Gelöst hab ich das Problem indem ich den Scheinwerfer etwas höher eingestellt habe. Auch das die Lampe sehr klein ist finde ich hier von Vorteil. Auf einer Gabel für 20 Zoll Räder sieht die nicht so wuchtig aus. Alles in allem macht die ein ziemlich gutes Licht für einen gut 50 Euro teuren, oder mit 50 Lux, günstigen Scheinwerfer.Für das Rücklicht haben wir auch zu Busch und Müller gegriffen. Das Toplight Flat S Plus ist ein guter Rückstrahler für einen guten Preis. Das besondere an dem Strahler ist die Bauweise. Der ist wirklich extrem flach (nur 20mm) und passt super unter die „Lippe“ des Gepäckträgers. Hinzukommt das speziell ausgeformte Linsensystem. So „projiziert“ eine einzelne LED einen breiten, gut sichtbaren Streifen quer durch den Strahler.

## [Update 2]
Der „Stinker-Porsche“ braucht einen neuen Sattel. Montiert ist ein Selle Royale Freedom. Meine Fresse, ist das ein Scheißding. Alter Schwede, auf dem Sattel wird sogar ich schwanger! Stilecht, für ein Hollandrad, muss dann wohl ein Sattel von Lepper drauf. Weil meine bessere Hälfte auch mit dem Rad fährt wird es wohl der Drieveer 90 werden weil der Unisex ist.

## [Update 3]
so…. der erste dicke Herbstwind ist vorbei und ich muss sagen dass das Fahrad ein ganz schöner Windfänger ist wenn das Regendach verbaut ist.

## [Update 4]
Mittlerweile hat das Lastenrad auch neue Schlappen bekommen. Vorne und hinten kamen nagelneue Schwalbe Marathon Spezial drauf. Und wenn ich schon mal dabei war, es wurden auch gleich neue Schläuche eingebaut, die Schaltung mal vernünftig eingestellt und den Scheinwerfer (s.o.) mit richtigen Unterlegscheiben befestigt. Riesen Dank an Armin von hallofahrrad* in dessen Werkstatt ich schrauben konnte.Wie man auf dem Bild rechts sehen kann konnte ich dort Hoch professionell daran schrauben. Der Porsche passte in keinen Montageständer und wurde kurzerhand auf einen improvisierten Bock gestellt. Ging erstaunlicherweise sogar richtig rückenfreundlich 🙂