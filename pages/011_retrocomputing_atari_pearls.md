# Atari Software Pearls
Here are some some pieces of software i have found over the internet. As the lazy dog as i am, i’m just copying an extract of the packaged readme’s

Download: Sysinfo

### SysInfo
Sysinfo is a utility to diagnose system level conflicts and problems. This Versions is available in english. Online ST-Guide help is available for any problems encountered using the program. Sysinfo runs on all Atari compatibles and emulators with at least a minimum resolution of 640×200. Some information can be updated by a programable system timer and it’s also possible to have the output dumped into a file.

### Resource Master
The program I will be reviewing for you today is called Resource Master. The version is 3.2 demo and it was created in 2000. It is (currently?) being distributed by Application Systems Heidelberg. Resource Master is somewhat of your typical GEM .RSC file editor. If you are confused as to what a resource file is, you can simply click here.

Although I am not very qualified to make an intensive review of the functionality of this program, what I do intend to give you is somewhat of an overview of the program. I will also start a task commonly done such as a language conversion of an RSC and report on the ease of use. The first thing I notice when opening RSM, is that the feel holds very true to the ever popular Interface resource editor.

Interface is a rather old editor though which does not support current AES features amongst other things. In fact, resource master holds so true to Interface, that it even has the same built in minidesktop. Doing something like a language conversion is just as easy and/or difficult as it was using Interface. I find using this program to be a breeze.

Current features available to programmers like bubbleGEM are used and used well. ItØs also easy to see placeholders like spaces in the text fields creating an easy method for which to match up spaces. All in all, from my best judgement it looks as though Resource Master is a spectacular utility able to compliment any programmer’s collection.

Download: Resource Master

### Adrenaline Ripper
Probably the most comprehensive music ripper which recognises the most formats (over 30). Excellent GFX ripper and the ability to un-compress many packed files. Excellent! It was made by the french demogroup Adrenaline.

Download : Adrenaline Ripper

## My ports for m68k
This parte is rather historic. I’ve found it on an old HDD and put it back here (i dont have the downloads at hand)

Here are some binary packages i have build from the original unix / linux sources to the m68k-atari-mint platform. All RPM files are packed .tar.gz files and converted with alien to rpm. I am using Fedora 8 Linux, Aranym and EasyMint to build and to test. I’ve tested it on my Aranym setup, i do not know how it works on a real Atari ( i have no real one atm ). Please let me know if it works or not. A word about MagiC. These are MiNT binaries and it will not work with MagiC.

### Joe’s Own Editor
This is my port of „Joe’s Own Editor“ to the the Atari/MiNT Platform. Have a look at its own website for the original source packages and to get some more informations about it.

* joe-3.5-m68k-atari-mint.rpm
* joe-3.5-m68k-atari-mint.tar.gz ( mirror -> ftp://kurobox.serveftp.net:3021/mint/editors/joe-3.5-m68k-atari-mint.tar.gz)

### GNU CVS 1.11.22
As i said, i have build a much more rescent version of cvs and here are the files i’ve made. You can grab the source from GNU’s FTP Server.

* cvs-1.11.22.rpm
* cvs-1.11.22.tar.gz

### GNU less 418
After building cvs ( see above ) i have also compiled a newer less. Like cvs you can grab the source from GNU’s FTP Server.

* less-418.rpm
* less-418.tar.gz

### Pack-Ice
Time to do some usefull. Pack-Ice! It is a ice compatible compression utility widely used in the demo scene done by the nocrew.

* packice-68000.tar.gz
* packice-68000.rpm
* packice-68030.tar.gz
* packice-68030.rpm

### File 4.24
While working with different files, i have discovered that the unix / linux command line tool file is missing in the EasyMint distribution. file determines a format of a file and give some user readable informations about it. For more informations and the sourcecode take a look at its website.

* file-4.24.tar.gz
* file-4.24.rpm

### dialog
From its website: „Dialog is a utility to create nice user interfaces to shell scripts, or other scripting languages, such as perl. It is non-graphical (it uses curses) so it can be run in the console or an xterm.“

* dialog-1.1-20080316.tar.gz ( mirror -> ftp://kurobox.serveftp.net:3021/mint/misc/dialog-1.1-20080316.tar.gz)
* dialog-1.1-20080316.rpm ( mirror -> ftp://kurobox.serveftp.net:3021/mint/misc/dialog-1.1-20080316.rpm)

