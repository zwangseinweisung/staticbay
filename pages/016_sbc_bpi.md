# BananaPi
Ich hab bei mir zuhause eine etwas ältere Mini-ITX Kiste stehen die nur als MPD dient und ständig durch läuft. Weil der Rechner aber gut und gerne etwa 60 Watt Strom frisst war ich schon etwas länger auf der Suche nach einer Stromsparenden alternative. Die von mir geliebten Beaglebone Black haben leider einen nachteil, sie haben Audio nur über HDMI und fielen deswegen aus dem Rennen. Weil ich aber keinen HDMI-Audio Splitter oder eine USB Soundkarte verwenden will fiel meine Wahl dann zunächst auf den Cubietruck. Da der aber im Moment nicht Lieferbar ist entschied ich mich dann zu einem Banana Pi.

Ich muss sagen, was die chinesische Firma LeMaker da abliefert ist schon große Klasse. Ein fast kompatibler Raspberry Pi Clone mit einer Allwiner A20 dualcore CPU, 1GB DDR3 Ram, Gigabit Netzwerk und der Oberknaller überhaupt, einem SATA-Anschluss. Der BPI ist in der Lage einen 2.5″ Platte (oder SSD) mit Strom zu versorgen. Kabel dran und läuft. Größere 3.5″ Platten brauchen wegen der 12V ein externes Netzteil. Speaking of Netzteil. Der BPI wird über USB befeuert und benötigt ein Netzteil von mindestens 1 Ampere. Will man noch eine SSD betreiben sollten es mindestens 1.5 Ampere sein. Daher scheiden „normale“ USB-Netzteile vom Handy aus, die liefern nämlich in der Regel nur 500 Milliampere.

## Ersatz als MPD

An der Hardware gab es nicht wirklich spektakuläres zu schrauben.  Das einzig erwähnenswerte ist wohl die gelaserte Trägerplatte die den BPI und die SSD miteinander verbindet. Mit Hilfe von Inkscape und einem Messschieber war die Vorlage für den Laser schnell zusammen geklickt. Aus Resten einer Plexiglasplatte, die wir im Chaosdorf noch rumliegen hatten war die Trägerplatte dann auch schnell ausgeschnitten. Die SSD und der BPI wurden dann mit M3-Schrauben gegeneinander verschraubt.

Weil auf dem BPI ein Debian läuft ist da auch nichts spannendes zu erwarten, Image runter laden, auf eine SD-Karte schreiben und starten. Anschließend noch kurz das System mit bananian-config einrichten, SD-Karte vergrößern und neu starten. So einfach kann es gehen.

Damit man aber noch Sound auf dem Kopfhörer hat muss man Alsa ein wenig anpassen. Dazu verändert man in der Datei /etc/asound.conf folgende Zeilen:

```bash
pcm.!default {
	type hw
	card 0 # for headphone, turn 1 to 0
	device 0
}
ctl.!default {
	type hw
	card 0 # for headphone, turn 1 to 0
}
```

Womit ich allerdings ständig, also immer auf Kriegsfuß stehe ist die Konfiguration vom MPD. Allerdings ging das dieses mal erstaunlich einfach. Es muss lediglich die Datei /etc/mpd.conf angepasst werden:

```bash
music_directory		/music/musik
playlist_directory	/var/lib/mpd/playlists
db_file			/var/lib/mpd/mpd.db
log_file		/tmp/mpd.log
pid_file		/run/mpd/pid
state_file		/var/lib/mpd/state
sticker_file		/var/lib/mpd/sticker.sql
user			mpd
group			audio
port			6600
id3v1_encoding		UTF-8
 
input {
	plugin 		curl
}
 
audio_output {
	type            alsa
	name            My ALSA Device
	mixer_type      software
}
```

## Fernbedienung
Der BPI hat noch einen Infrarotempfänger der sich wunderbar dazu nutzen lässt das System mit fernzusteuern.
Lirc ist schon installiert und ich werde mich die Tage damit beschäftigen den auf mein „Zepter“ zu trainieren.

Außerdem muss ich nochmal das Sata-Kabel tauschen. Das was jetzt grade dran ist, ist viel zu lang. Irgendwo hab ich noch ein ganz kurzes. Das muss ich erstmal finden 🙁