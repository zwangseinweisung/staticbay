# Beaglebone – BeagleWeather
Hin und wieder beschäftige ich mich mit dem Thema Wetter und weil die meisten professionellen Wetterstationen nicht in meinem direkten Umfeld sind (wer hat das schon?) und ich noch einen Beaglebone Black über hatte hab mich mich dazu entschieden eine eigene Wetterstation zusammen zu bauen. Außerdem befasse ich mich gerade damit ein Anemometer zu bauen das mir die Windgeschwindigkeit und Windrichtung anzeigt. Dazu aber später mehr wenn das Ding fertig gebaut sind.

Wie dem auch sei, so sieht derzeit meine Konfiguration aus:

* Beaglebone Black mit Debian Linux
* 2* DS1820
* DHT11 Lufteuchtigkeitssensor
* BMP085 Barometer

## Hardware:
## Temperaturerfassung
Kern meiner Station zur Temperaturerfassung sind die beiden DS1820 die ganz einfach per i²c angebunden werden. Diese kleinen Dinger sind nicht sehr kompliziert, im einfachsten Fall wird einfach plus und minus (VCC u. GND) sowie die Datenleitung an den Beaglebone angeschlossen und schon läufts. Das unheimlich praktische an denen ist das die eine eindeutige ID haben und es deswegen sehr einfach ist mehrere von denen parallel zu schalten.

Einige Quellen sind der Meinung das man einen Widerstand zwischen VCC und DATA anschließen muss, andere wiederum nicht (Pullup 4,7kΩ oder 10kΩ). Elektrisch macht das bestimmt irgendwas aber ich konnte beim Aufbau keinen Messunterschied darin erkennen und hab die dann einfach weg gelassen. 2 Lötpins weniger 😉

### Luftfeuchteerfassung
Etwas kniffeliger wurde es beim Anschließen des DHT11. Leider lief der bei mir am Beaglebone nicht richtig, dafür aber an einem ATMEGA 8-16 PU. Das war schon etwas komplizierter aber nicht unmöglich. Auf dem Arduino Playground fand sich schnell ein passendes Prögrammchen das auf dem ATMEGA angepasst wurde und lief. Der Rest war einfach. Beagle und und ATMEGA kommunizieren über die Serielle Schnittstelle die beide haben.

Aus reiner Faulheit wird die Luftfeuchtigkeit noch nicht mitgeloggt. Kommt aber sehr bald™.

### Luftdruckerfassung
Um den Luftdruck kümmert sich ein BMP085 der wieder an den Beagle angeschlossen wurde. Der spricht auch wieder i²c und ist dementsprechend leicht an den Beagle (wie die DS1820 oben) anzuschließen. Das Thermometer dass das Teil mitbringt benutze ich nicht. Eine besonderheit hat der Sensor aber noch. Der gibt nicht den Wert in Millibar aus sondern Millibar*100. D.h. dass man in der Software (komm ich später zu) den Wert runter teilen muss.

### Verpackung
Die Außensensoren hab ich in eine schlichte Abzweigdose aus dem Elektrobereich gepackt. Das schützt ausreichen die Elektronik vor Regen und so weiter. Außerdem hat die Abzweigdose 2 praktische Laschen mit Löchern zum befestigen 😉

Von unten habe ich ein großes Loch reingebohrt um ein Netzwerkkabel durch zu schieben, von oben ein kleineres um den Luftdruck zu erfassen. Der Temperatursensor liegt im Moment noch innerhalb der Abzweigdose. Das war keine gute Idee. Bei direkter Sonneneinstrahlung kann man sehr gut auf dem Graphen sehr gut erkennen das es in der Dose doch recht warm wird und das die Messung verfälscht.

### Platine
Um alles miteinander zu verbinden habe ich mir mit Eagle eine kleine Platine erstellt und bei uns im Chaosdorf geätzt. Leider hat sich da ein kleiner Fehler eingeschlichen. Ich hatte den falschen UART vom Beagle erwischt und hatte erstmal keine Messwerte vom Feuchtigkeitssensor. Kein Beinbruch, Leiterbahn aufgekratzt und 2 dünne Käbelchen verlegt. Danach ging es.

## Software
Bald™, gibt es hier die Software zu meinem BeagleWeather Projekt

Update: Die Files sind bei GitHub zu finden: https://github.com/ragnar76/bbb-ds1820