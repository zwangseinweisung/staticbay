# Singleboard Computing

Single Board Computer oder auch Einplatinencomputer sind kleine, meist in der Größe einer Kreditkarte, die alles in sich vereinen. Da ist dann die CPU, RAM und Schnittstellen drauf. Perfekt um dinge zu Automatisieren oder damit zu experementieren. Bei mir laufen 2 Verschiedene SBCs. Einmal der Banana Pi den ich als Fileserver benutze und einmal der der Beaglebone Black der bei mir als Wetterstation läuft.

Auf dieser Seite habe ich die verschiedenen Projekte mal zusammengefasst.

## Raspi
* Raspi: Wetterstation mit LC-Display (noch nicht dokumentiert)
* Raspi: HD-Kamera (noch nicht dokumentiert)
* Raspi: Pi-Hole (noch nicht dokumentiert)
* Raspi: Diverses (noch nicht dokumentiert)

## Banana Pi
* Banana PI ( -> https://theragnarbay.org/bananapi/)
* Banana Pi: Datengrab ( -> https://theragnarbay.org/bananapi-datengrab/)
* Banana Pi: Fernbedienung ( -> https://theragnarbay.org/bananapi-fernbedienung/)

## Beaglebone Black
* Beaglebone Black: Wetterstation ( -> https://theragnarbay.org/beaglebone-beagleweather/)
* Beaglebone Black: Wetterstation – Hardware ( -> https://theragnarbay.org/beaglebone-beagleweather/)
* Beaglebone Black: Wetterstation – Software ( -> https://theragnarbay.org/beaglebone-beagleweather/)

Ich hab schon wieder eine neue Idee, weitere Infos kommen wenn es soweit ist 🙂
