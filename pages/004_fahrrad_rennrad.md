# Rennrad – Wheeler Sirmione
Mein „Stadtflitzer“. Mit dem fahr ich jeden morgen zur Umschulung Arbeit (für die, die es interessiert: Fachinformatiker, Systemintegration) nach Essen fahre. Ist zwar ein Stahlrahmen aber trotzem noch leichter als mein „Langstreckenfahrzeug“. Perfekt um es „mal eben“ auf die Schulter zu schmeißen und die Treppe vom/zum Zug zu nehmen.

* Baujahr: irgendwann zwischen 1990 und 1999
* Rahmen: Stahl, 55cm (gemessen von mitte Trelager bis Ende Sattelrohr)
* Baugruppe: Shimano Exage 300EX
* Felgen: Mavic 622 x 15
* Mäntel: Schwalbe Marathon 700x23C
* Sattel: Brooks B17 Flyer